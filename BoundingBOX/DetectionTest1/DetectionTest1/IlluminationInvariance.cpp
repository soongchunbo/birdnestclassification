#include "stdafx.h"
#include "IlluminationInvariance.h"



void IISubGammaCorrelation(Mat srcImg, Mat dstImg32f, double gamma)
{
	int i, j;

	for(i=0; i<srcImg.rows; i++)
	{
		for(j=0; j<srcImg.cols; j++)
		{
			//CV_IMAGE_ELEM(dstImg32f, float, i, j) = 
			//	(float) pow( CV_IMAGE_ELEM( srcImg, uchar, i, j ), gamma );
			dstImg32f.at<float>(Point(j,i)) = 
				(float) pow((double)srcImg.at<uchar>(Point(j,i)), gamma);
		}
	}
}

void IISubDOG(Mat srcImg32f, Mat dstImg32f, double sigma0, double sigma1)
{
	int i, j;
	Mat gaussian0Img32f;
	Mat gaussian1Img32f;
	int size0, size1;

	size0 = (int)(6 * sigma0 + 1);
	size1 = (int)(6 * sigma1 + 1);
	//gaussian0Img32f = cvCreateImage(cvGetSize(srcImg32f), IPL_DEPTH_32F, 1);
	//gaussian1Img32f = cvCreateImage(cvGetSize(srcImg32f), IPL_DEPTH_32F, 1);
	gaussian0Img32f = Mat(srcImg32f.rows, srcImg32f.cols, CV_32FC1);
	gaussian1Img32f = Mat(srcImg32f.rows, srcImg32f.cols, CV_32FC1);

	cvSmooth(&srcImg32f, &gaussian0Img32f, CV_GAUSSIAN, size0, size0, sigma0);
	cvSmooth(&srcImg32f, &gaussian1Img32f, CV_GAUSSIAN, size1, size1, sigma1);
	

	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//CV_IMAGE_ELEM(dstImg32f, float, i, j) =
			//	CV_IMAGE_ELEM(gaussian0Img32f, float, i, j) - CV_IMAGE_ELEM(gaussian1Img32f, float, i, j);
			dstImg32f.at<float>(Point(j,i)) = (float)gaussian0Img32f.at<uchar>(i,j) - (float)gaussian1Img32f.at<uchar>(i,j);
		}
	}

	//cvReleaseImage(&gaussian0Img32f);
	//cvReleaseImage(&gaussian1Img32f);
	gaussian0Img32f.release();
	gaussian1Img32f.release();
}

void IISubContrastEqualization(Mat srcImg32f, Mat dstImg32f, double alpha, double toe)
{
	int i, j;
	double mean;

	mean = 0;
	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//mean += pow( (double)abs(CV_IMAGE_ELEM(srcImg32f, float, i, j)), alpha );
			mean += pow((double)abs((float)srcImg32f.at<uchar>(i,j)), alpha);
		}
	}
	mean = mean / srcImg32f.cols / srcImg32f.rows;
	mean = pow( mean, 1.0 / alpha );
	mean = 1.0 / mean;
	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//CV_IMAGE_ELEM(dstImg32f, float, i, j) = (float)(CV_IMAGE_ELEM(srcImg32f, float, i, j) * mean);
			dstImg32f.at<float>(i,j) = (float)(srcImg32f.at<uchar>(i,j) * mean);
		}
	}

	mean = 0;
	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//mean += MIN( toe, pow( (double)abs(CV_IMAGE_ELEM(dstImg32f, float, i, j)), alpha ) );
			mean += min(toe, pow((double)abs((float)dstImg32f.at<uchar>(i,j)), alpha));
		}
	}
	mean = mean / srcImg32f.cols / srcImg32f.rows;
	mean = pow( mean, 1.0 / alpha );
	mean = 1.0 / mean;
	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//CV_IMAGE_ELEM(dstImg32f, float, i, j) = (float)(CV_IMAGE_ELEM(dstImg32f, float, i, j) * mean);
			dstImg32f.at<float>(i,j) = (float)((float)(dstImg32f.at<uchar>(i,j)) * mean);
		}
	}

	for(i=0; i<srcImg32f.rows; i++)
	{
		for(j=0; j<srcImg32f.cols; j++)
		{
			//CV_IMAGE_ELEM(dstImg32f, float, i, j) = (float)toe * tanh( CV_IMAGE_ELEM(dstImg32f, float, i, j) / toe );
			dstImg32f.at<float>(i,j) = (float)toe * tanh((float)(dstImg32f.at<uchar>(i,j)/toe));
		}
	}
}

void IlluminationInvairanceProcedure(Mat srcImg, Mat dstImg32f)
{
	Mat tempImg32f;

	//tempImg32f = cvCreateImage(cvGetSize(dstImg32f), IPL_DEPTH_32F, 1);
	tempImg32f = Mat(dstImg32f.rows, dstImg32f.cols, CV_32FC1);
	IISubGammaCorrelation(srcImg, dstImg32f, DCV_ILLUMININVAR_DEFAULT_GAMMA);
	IISubDOG(dstImg32f, tempImg32f, DCV_ILLUMININVAR_DEFAULT_SIGMA0, DCV_ILLUMININVAR_DEFAULT_SIGMA1);
	IISubContrastEqualization(tempImg32f, dstImg32f, DCV_ILLUMININVAR_DEFAULT_ALPHA, DCV_ILLUMININVAR_DEFAULT_TOE);

	//cvReleaseImage(&tempImg32f);
	tempImg32f.release();
}