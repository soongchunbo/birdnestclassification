#include "StdAfx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>


using namespace std;
using namespace cv;

#define LOWTHRESHOLD 50
#define UPPERTHRESHOLD 200

Mat GetCannyEdgeMat(Mat src);

Mat GetLaplacianMat(Mat src, int depth, int ksize);