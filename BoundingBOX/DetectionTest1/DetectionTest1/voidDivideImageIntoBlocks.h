#include "StdAfx.h"

#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>


using namespace std;
using namespace cv;


class BLOCK
{
public:
	string b_source;
	int b_width;
	int b_height;

	int blockLBPcode;
	//vector<int> lbpcode;


	BLOCK();
	BLOCK(string source, int width, int height);

	~BLOCK();

	void voting(Mat block);

	//calculate the distance
	double calcDistance();
};


