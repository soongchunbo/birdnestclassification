#ifndef DCV_BG_PROJECT_FACERECOGNITION_ILLUMINATION
#define DCV_BG_PROJECT_FACERECOGNITION_ILLUMINATION


#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <algorithm>
#include "cv.h"
using namespace std;
using namespace cv;

#define DCV_ILLUMININVAR_DEFAULT_GAMMA 0.2
#define DCV_ILLUMININVAR_DEFAULT_SIGMA0 1.0
#define DCV_ILLUMININVAR_DEFAULT_SIGMA1 2.0
#define DCV_ILLUMININVAR_DEFAULT_ALPHA 1.0
#define DCV_ILLUMININVAR_DEFAULT_TOE 10

#define NOMINMAX

void IlluminationInvairanceProcedure(Mat srcImg, Mat dstImg32f);

#endif