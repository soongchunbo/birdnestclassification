#include <string>
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

class WinInfo
{
public:
	Size size;
	Point lefttop;
	Mat source;
	int layer;
	double factor;
	Mat win;
	Mat resizedSource;

	WinInfo();
	WinInfo(Size s, Point lt, Mat img, int layer_count, double f);
	~WinInfo();

	bool _checkWindow();
	void _updateLayer(int inputLayer);
	void _updateLocation(Point p);
	Mat _updateSource();
	Mat _extractWindow(Mat input); //input is the output of _updateSource()

	Rect _getRect();
	Mat _drawRect(Rect rect, Mat orgImg);
	//float predict(CvSVM SVM);
};