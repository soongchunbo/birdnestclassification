#include "stdafx.h"
#include "WinInfo.h"

WinInfo::WinInfo()
{
	size = Size(100,100);
	lefttop = Point(0,0);
	source = imread("C:/Course/C plusplus test/hogtest/hogtest/44.jpg", 1);
	layer = 0;
	factor = 0.9;
	resizedSource = source;
}

WinInfo::WinInfo(Size s, Point lt, Mat img, int layer_count, double f)
{
	size = s;
	lefttop = lt;
	source = img;
	layer = layer_count;
	factor = f;
	resizedSource = source;
}

bool WinInfo::_checkWindow()
{
	if(win.rows < resizedSource.rows && win.cols < resizedSource.cols)
		return true;
	else
		return false;
}

void WinInfo::_updateLayer(int inputLayer)
{
	layer = inputLayer;
}

void WinInfo::_updateLocation(Point p)
{
	lefttop = p;
}

Mat WinInfo::_updateSource()
{
	Mat resizedSource;
	int width = int((double)source.cols * pow(factor, double(layer)));
	int height = int((double)source.rows * pow(factor, double(layer)));

	resize(source, resizedSource, Size(width, height));
	this->resizedSource = resizedSource;

	return resizedSource;
}

//input is the output of _updateSource()
Mat WinInfo::_extractWindow(Mat input)
{
	Mat output;

	if(_checkWindow())
	{
		Rect tempImg(lefttop.x, lefttop.y, size.width, size.height);
		output = resizedSource(tempImg);
		win = output;
		return output;
	}
	else
	{
		cout << "Window size error" << endl; 
		return output;
	}
}

Rect WinInfo::_getRect()
{
	int winHeight = size.height;
	int winWidth = size.width;

	int rectHeight = int((double)winHeight/pow(factor, double(layer)));
	int rectWidth = int((double)winWidth/pow(factor, double(layer)));

	int location_x = int((double)lefttop.x/pow(factor, double(layer)));
	int location_y = int((double)lefttop.y/pow(factor, double(layer)));

	Rect rect(location_x, location_y, rectWidth, rectHeight);

	return rect;
}

Mat WinInfo::_drawRect(Rect rect, Mat orgImg)
{
	Mat sourceImg = orgImg;
	rectangle(sourceImg, rect, Scalar(255,0,0),3,8,0);

	//imshow("test", sourceImg);
	//waitKey(0);

	return sourceImg;
}

WinInfo::~WinInfo()
{
	source.release();
	win.release();
	resizedSource.release();
}