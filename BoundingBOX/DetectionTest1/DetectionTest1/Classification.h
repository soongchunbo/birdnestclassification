#include "StdAfx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>


using namespace std;
using namespace cv;

void SVMClassification(Mat input, Mat inputlabel, CvSVMParams params);

void TrainRandomForestClassification(Mat input, Mat inputlabel, CvRTParams rfParams);