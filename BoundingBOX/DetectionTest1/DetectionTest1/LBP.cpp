#include "StdAfx.h"
#include "LBP.h"

void getLBP(Mat inputMat, Mat outputMat, int radius)
{
	//so far, radius is equal to 1.
	if(radius == 1)
	{
		cout << "radius: " << radius << endl;
		Point2i cPoint;
		uchar cintensity;
		uchar nintensity[8];
		uchar assignValue;

		for(int i = 1; i < inputMat.rows - 1; i++)
		{
			for(int j = 1; j < inputMat.cols - 1; j++)
			{
				cPoint.x = j;
				cPoint.y = i;

				cintensity = inputMat.at<uchar>(Point(j,i));

				
				nintensity[0] = inputMat.at<uchar>(i - 1, j - 1);
				nintensity[1] = inputMat.at<uchar>(i - 1, j);
				nintensity[2] = inputMat.at<uchar>(i - 1, j + 1);
				nintensity[3] = inputMat.at<uchar>(i, j + 1);
				nintensity[4] = inputMat.at<uchar>(i + 1, j + 1);
				nintensity[5] = inputMat.at<uchar>(i + 1, j);
				nintensity[6] = inputMat.at<uchar>(i + 1, j - 1);
				nintensity[7] = inputMat.at<uchar>(i, j - 1);

				assignValue = 0;
				for(int k = 0; k < 8; k++)
				{
					if(nintensity[k] >= cintensity)
					{
						nintensity[k] = pow(2.0, 7-k);
					}
					else
					{
						nintensity[k] = 0;
					}
					assignValue += nintensity[k];
				}
				
				outputMat.at<uchar>(i - 1, j - 1) = assignValue;

			}
		}
	}
	else
	{
		////////
		////////
	}
}

void getCSLBP(Mat inputMat, Mat outputMat)
{
	uchar cintensity;
	double nintensity[4];
	double assignValue;

	for(int i = 1; i < inputMat.rows - 1; i++)
	{
		for(int j = 1; j < inputMat.cols - 1; j++)
		{
			nintensity[0] = (inputMat.at<uchar>(i, j + 1) - inputMat.at<uchar>(i, j - 1))/255.0;
			nintensity[1] = (inputMat.at<uchar>(i + 1, j + 1) - inputMat.at<uchar>(i - 1, j - 1))/255.0;
			nintensity[2] = (inputMat.at<uchar>(i + 1, j) - inputMat.at<uchar>(i - 1, j))/255.0;
			nintensity[3] = (inputMat.at<uchar>(i + 1, j - 1) - inputMat.at<uchar>(i - 1, j + 1))/255.0;
			//cout << double(nintensity[0]) << " " << double(nintensity[1]) << " " << double(nintensity[2]) << " " << double(nintensity[3]) << endl;
			assignValue = 0.0;
			for(int k = 0; k < 4; k++)
			{
				if(fabs(nintensity[k]) > 0.4)
				{
					nintensity[k] = pow(2.0, k*1.0);
				}
				else
				{
					nintensity[k] = 0.0;
				}
				
				assignValue += nintensity[k];
			}
			//cout << int(assignValue) << endl;
			outputMat.at<uchar>(i - 1, j - 1) = assignValue*255.0;
		}
	}
}


//Deside if the position is uniformed
bool isUniform(int nSample, int index)
{
	int i;
	int* binary_pattern;
	int temp;

	binary_pattern = (int*)malloc(sizeof(int) * nSample);

	for(i=(nSample-1);i>=0;i--)
	{
		temp = (int)pow(2.0,i);
		binary_pattern[i] = index / temp;
		index = index - binary_pattern[i]*temp;
	}

	int count = 0;
	for(int i=1;i<nSample;i++)
	{
		if(binary_pattern[i-1]!=binary_pattern[i])
		{
			count++;
		}
	}

	if(count>2)
	{
		return false;
	}
	else
	{
		return true;
	}
}


int* CreateUniformMappingTable(int nSample, int* codeCount)
{
	int i;
	int index;
	int* mapTable;
	int posCount;

	index = 1;
	posCount = (int)pow(2.0, nSample);
	mapTable = (int*)malloc(sizeof(int) * posCount);
	for(i=0; i<posCount; i++)
	{
		if(isUniform(nSample, i))
		{
			mapTable[i] = index;
			index ++;
		}
		else
		{
			mapTable[i] = 0;
		}
	}

	(*codeCount) = index;
	return mapTable;
}

void InitializeLBPModel(LBPModel* lbpModel)
{
	if(lbpModel->uniformed == 1)
	{
		lbpModel->mappingTable = CreateUniformMappingTable(lbpModel->nNeighbor, &lbpModel->codeLength);
	}
	else
	{
		lbpModel->codeLength = (int)pow(2.0, lbpModel->nNeighbor);
	}

	if(lbpModel->type == lbpModel->DCV_LBPTYPE_TERNARY ||
		lbpModel->type == lbpModel->DCV_LBPTYPE_SCALEINVARIANT_TERNARY)
	{
		lbpModel->codeLength *= 2;
	}
}

void ReleaseLBPModel(LBPModel* lbpModel)
{
	if(lbpModel->uniformed == 1)
	{
		free(lbpModel->mappingTable);
	}
}



void TraverseMatValue(Mat* lbpMat)
{
	for(int i = 0; i < (*lbpMat).rows; i++)
	{
		for(int j = 0; j < (*lbpMat).cols; j++)
		{
			cout << "LBP value: " << (*lbpMat).at<int>(i,j) << endl;
		}
	}

}





Mat* GetLBPMap(Mat srcImg32f, LBPModel* lbpModel, vector<vector<int>>& LBPcollection)
{
	int i, j, k;
	int xAxis, yAxis;
	Mat* lbpMat;
	float cVal;
	float* vals;
	int lbpVal;
	int lbpVal2;
	///
	///Mat lbpinterMat(srcImg32f.rows-2, srcImg32f.cols-2, CV_8UC1);
	///

	if(lbpModel->type == lbpModel->DCV_LBPTYPE_BINARY)
	{
		lbpMat = new Mat(srcImg32f.rows - lbpModel->radius * 2, srcImg32f.cols - lbpModel->radius * 2, CV_32FC1);
		
	}
	else
	{
		lbpMat = new Mat(srcImg32f.rows - lbpModel->radius * 2, srcImg32f.cols - lbpModel->radius * 2, CV_32FC2);
	}

	vals = (float*)malloc(sizeof(float) * lbpModel->nNeighbor);
	if(lbpModel->type == lbpModel->DCV_LBPTYPE_BINARY)
	{
		
		for(i = (int)lbpModel->radius; i < srcImg32f.rows - lbpModel->radius; i++)
		{
			/////////////
			vector<int> tempLBP;
			/////////////
			for(j = (int)lbpModel->radius; j < srcImg32f.cols - lbpModel->radius; j++)
			{
				//center value				
				cVal = srcImg32f.at<uchar>(Point(j,i));

				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					xAxis = cvRound(j + lbpModel->radius * cos(2* CV_PI * k / lbpModel->nNeighbor));
					yAxis = cvRound(i - lbpModel->radius * sin(2* CV_PI * k / lbpModel->nNeighbor));
					//vals[k] = CV_IMAGE_ELEM((IplImage*)&srcImg32f, float, yAxis, xAxis);
					
					vals[k] = (float)srcImg32f.at<uchar>(Point(xAxis, yAxis));
					
				}

				//get lbp value
				lbpVal = 0;
				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					if(vals[k] > cVal)
					{
						lbpVal = ( (lbpVal >> (lbpModel->nNeighbor - k - 1)) | 1 ) << (lbpModel->nNeighbor - k - 1);
						
					}
				}
				tempLBP.push_back(lbpVal);

				if(lbpModel->uniformed == 0)
				{
					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)) = lbpVal;
					(*lbpMat).at<int>(Point((int)(j - lbpModel->radius), (int)(i - lbpModel->radius))) = lbpVal;
					
					//lbpinterMat.at<uchar>((int)(j - 1), (int)(i - 1)) = lbpVal;
				}
				else
				{
					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)) = lbpModel->mappingTable[lbpVal];
					(*lbpMat).at<float>(Point((int)(j - lbpModel->radius), (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal];
					//lbpinterMat.at<int>(Point((int)(j - lbpModel->radius), (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal];
				}
			}
			//////////////////////////////////////////
			LBPcollection.push_back(tempLBP);
			
			//tempLBP.clear();
			//////////////////////////////////////////
		}
		
	}
	else if(lbpModel->type == lbpModel->DCV_LBPTYPE_TERNARY)
	{
		for(i = (int)lbpModel->radius; i < srcImg32f.rows - lbpModel->radius; i++)
		{
			////////
			vector<int> tempLBP;
			////////
			for(j = (int)lbpModel->radius; j < srcImg32f.cols - lbpModel->radius; j++)
			{
				//get center value
				//cVal = CV_IMAGE_ELEM((IplImage*)&srcImg32f, float, i, j);
				cVal = srcImg32f.at<uchar>(Point(j,i));

				//get neighbor values
				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					xAxis = cvRound(j + lbpModel->radius * cos(2 * CV_PI * k / lbpModel->nNeighbor));
					yAxis = cvRound(i - lbpModel->radius * sin(2 * CV_PI * k / lbpModel->nNeighbor));
					//vals[k] = CV_IMAGE_ELEM((IplImage*)&srcImg32f, float, yAxis, xAxis);
		
					vals[k] = (float)srcImg32f.at<uchar>(Point(xAxis, yAxis));
					
				}

				lbpVal = 0;
				lbpVal2 = 0;
				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					if(vals[k] > cVal + lbpModel->ternaryRange)
					{
						lbpVal = ( (lbpVal >> (lbpModel->nNeighbor - k - 1)) | 1 ) << (lbpModel->nNeighbor - k - 1);
						
						//cout << "get lbp value2: " << lbpVal << endl;
					}
					else if(vals[k] < cVal - lbpModel->ternaryRange)
					{
						lbpVal2 = ( (lbpVal2 >> (lbpModel->nNeighbor - k - 1)) | 1 ) << (lbpModel->nNeighbor - k - 1);
						//
					}
				}
				tempLBP.push_back(lbpVal);
				//tempLBP.push_back(lbpVal2);

				if(lbpModel->uniformed == 0)
				{
					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)*2) = lbpVal;
					(*lbpMat).at<float>(Point((int)(j - lbpModel->radius)*2, (int)(i - lbpModel->radius))) = lbpVal;

					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)*2 + 1) = lbpVal;
					(*lbpMat).at<float>(Point((int)(j - lbpModel->radius)*2 + 1, (int)(i - lbpModel->radius))) = lbpVal;
				}
				else
				{
					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)*2) = lbpModel->mappingTable[lbpVal];
					(*lbpMat).at<float>(Point((int)(j - lbpModel->radius)*2, (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal];

					//CV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)(j - lbpModel->radius)*2 + 1) = lbpModel->mappingTable[lbpVal2];
					(*lbpMat).at<float>(Point((int)(j - lbpModel->radius)*2 + 1, (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal2];
				}
			}
			//////////////////////////////////////////
			LBPcollection.push_back(tempLBP);
			//tempLBP.clear();
			//////////////////////////////////////////
		}
	}
	else if(lbpModel->type == lbpModel->DCV_LBPTYPE_SCALEINVARIANT_TERNARY)
	{
		for(i = (int)lbpModel->radius; i<srcImg32f.rows - lbpModel->radius; i++)
		{
			////////
			vector<int> tempLBP;
			////////

			for(j = (int)lbpModel->radius; j < srcImg32f.cols - lbpModel->radius; j++)
			{
				//get center value
				//cVal = CV_IMAGE_ELEM((IplImage*)&srcImg32f, float, i, j);
				cVal = (float)srcImg32f.at<uchar>(Point(j,i));

				//get neighbor values
				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					xAxis = cvRound(j + lbpModel->radius * cos(2 * CV_PI * k / lbpModel->nNeighbor));
					yAxis = cvRound(i - lbpModel->radius * sin(2 * CV_PI * k / lbpModel->nNeighbor));
					//vals[k] = CV_IMAGE_ELEM((IplImage*)&srcImg32f, float, yAxis, xAxis);
					vals[k] = (float)srcImg32f.at<uchar>(Point(xAxis, yAxis));
				}

				//get lbp value
				lbpVal = 0;
				lbpVal2 = 0;
				for(k = 0; k < lbpModel->nNeighbor; k++)
				{
					if(vals[k] > cVal + cVal * lbpModel->scaleRatio)
					{
						lbpVal = ( (lbpVal >> (lbpModel->nNeighbor - k - 1)) | 1 ) << (lbpModel->nNeighbor - k - 1);
						
					}
					else if(vals[k] < cVal - cVal * lbpModel->scaleRatio)
					{
						lbpVal2 = ( (lbpVal2 >> (lbpModel->nNeighbor - k - 1)) | 1 ) << (lbpModel->nNeighbor - k - 1);
						
					}
				}
				tempLBP.push_back(lbpVal);
				//tempLBP.push_back(lbpVal2);

				if(lbpModel->uniformed == 0)
				{
					//DCV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)( j - lbpModel->radius ) * 2) = lbpVal;
					(*lbpMat).at<float>(Point((int)( j - lbpModel->radius ) * 2, (int)(i - lbpModel->radius))) = lbpVal;
					
					//DCV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)( j - lbpModel->radius ) * 2 + 1) = lbpVal2;
					(*lbpMat).at<float>(Point((int)( j - lbpModel->radius ) * 2 + 1, (int)(i - lbpModel->radius))) = lbpVal2; 
				}
				else
				{
					//DCV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)( j - lbpModel->radius ) * 2) = lbpModel->mappingTable[lbpVal];
					(*lbpMat).at<float>(Point((int)( j - lbpModel->radius ) * 2, (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal];
					
					//DCV_MAT_ELEM((CvMat)(*lbpMat), float, (int)(i - lbpModel->radius), (int)( j - lbpModel->radius ) * 2 + 1) = lbpModel->mappingTable[lbpVal2];
					(*lbpMat).at<float>(Point((int)( j - lbpModel->radius ) * 2 + 1, (int)(i - lbpModel->radius))) = lbpModel->mappingTable[lbpVal2];
				}
			}
			//////////////////////////////////////////
			LBPcollection.push_back(tempLBP);
			//tempLBP.clear();
			//////////////////////////////////////////
		}
	}
	/////////
	///imshow("lbpshow", lbpinterMat);
	///waitKey(0);
	//////////
	//TraverseMatValue(lbpMat);
	free(vals);
	return lbpMat; 
}


void ExtractLBPFeatureInRegion(Mat* lbpMat32f, Rect* region, LBPModel* lbpModel, double* feature)
{
	int i, j;

	if(lbpModel->type == lbpModel->DCV_LBPTYPE_BINARY)
	{
		for(i=region->y; i<region->y + region->height; i++)
		{
			for(j=region->x; j<region->x + region->width; j++)
			{
				//feature[ (int)CV_MAT_ELEM((CvMat)(*lbpMat32f), float, i, j) ] += 1;
				feature[(int)(*lbpMat32f).at<float>(Point(j,i))] += 1;
				//cout<< "x:" << j << " y:" << i << " LBP: " << (int)(*lbpMat32f).at<float>(Point(j,i)) << " feature count: " << feature[(int)(*lbpMat32f).at<float>(Point(j,i))] << endl;
			}
		}
	}
	else
	{
		for(i=region->y; i<region->y + region->height; i++)
		{
			for(j=region->x; j<region->x + region->width; j++)
			{
				//feature[ (int)DCV_MAT_ELEM((CvMat)(*lbpMat32f), float, i, j*2) ] += 1;
				feature[(int)(*lbpMat32f).at<float>(Point(j*2,i))] += 1;
				//cout << "test2: " << feature[(int)(*lbpMat32f).at<float>(Point(j*2,i))] << endl;

				//feature[ lbpModel->codeLength / 2 + (int)DCV_MAT_ELEM((CvMat)(*lbpMat32f), float, i, j*2 + 1) ] += 1;
				feature[lbpModel->codeLength / 2 + (int)(*lbpMat32f).at<float>(Point(j*2+1, i))] += 1;
				//cout << "test3: " << feature[lbpModel->codeLength / 2 + (int)(*lbpMat32f).at<float>(Point(j*2+1, i))] << endl;
			}
		}
	}
}




void NormalizeLBP(double* feature, int length)
{
	int i;
	double sum;

	sum = 0;
	for(i=0; i<length; i++)
	{
		sum += feature[i];
	}

	if(sum != 0)
	{
		for(i=0; i<length; i++)
		{
			feature[i] /= sum;
		}
	}
}


double* ExtractLBPFeature(Mat* lbpMat32f, int nRow, int nCol, LBPModel* lbpModel)
{
	int i, j;
	int ptr;
	double* feature;
	double subWidth, subHeight, xOffset, yOffset;
	Rect region;

	feature = (double*)malloc(sizeof(double) * nRow * nCol * lbpModel->codeLength);
	memset(feature, 0, sizeof(double) * nRow * nCol * lbpModel->codeLength);

	ptr = 0;
	subWidth = (double)(*lbpMat32f).cols / nCol;
	subHeight = (double)(*lbpMat32f).rows / nRow;
	xOffset = ( (*lbpMat32f).cols - subWidth * nCol ) / 2.0;
	yOffset = ( (*lbpMat32f).rows - subHeight * nRow ) / 2.0;
	for(i=0; i<nRow; i++)
	{
		for(j=0; j<nCol; j++)
		{
			region.y = (int)(yOffset + subHeight * i);
			region.x = (int)(xOffset + subWidth * j);
			region.width = (int)(subWidth);
			region.height = (int)(subHeight);
			
			ExtractLBPFeatureInRegion(lbpMat32f, &region, lbpModel, &feature[ptr]);
			NormalizeLBP(&feature[ptr], lbpModel->codeLength);
			ptr += lbpModel->codeLength;
		}
	}

	return feature;
}

double LBPDistance(double* feature1, double* feature2, int length)
{
	int i;
	double dist;

	dist = 0;
	for(i=0; i<length; i++)
	{
		if(feature1[i] + feature2[i] != 0)
		{
			dist += (feature1[i] - feature2[i]) * (feature1[i] - feature2[i]) / (feature1[i] + feature2[i]);
		}
	}

	return dist;
}

