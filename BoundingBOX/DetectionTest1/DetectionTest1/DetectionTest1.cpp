// DetectionTest1.cpp : Defines the entry point for the console application.
//
// test1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\nonfree\features2d.hpp>
#include <opencv2\ml\ml.hpp>
#include <iostream>
#include <ctime>
#include "LBP.h"
#include "IOOperation.h"
#include "voidDivideImageIntoBlocks.h"
#include "CannyEdgeDetection.h"
#include "WinInfo.h"
////test procedure///////
#include "TestingProcedure.h"
////test procedure end///

#define DCV_NORM_L1 0
#define DCV_NORM_L2 1

using namespace std;
using namespace cv;
using namespace cv::gpu;


//Funcation Abstract :
//	The EUCLIDEAN distance of two vectors
//Params :
//	{vec1 (in)} : the 1st vector
//	{vec2 (in)} : the 2nd vector
//	{dataLength (in)} : the length of the vector
//Return :
//	{double} : the distance value
double VectorDistance(double* vec1, double* vec2, int dataLength)
{
	int i;
	double rtnVal = 0;
	for(i=0;i<dataLength;i++)
	{
		rtnVal += (vec1[i] - vec2[i]) * (vec1[i] - vec2[i]);
	}
	rtnVal = sqrt(rtnVal);

	return rtnVal;
}

//Funcation Abstract :
//	The normalization procedure of a vector
//Params :
//	{vec (in/out)} : the data vector
//	{dataLength (in)} : the length of data in vector
//	{normKind (in)} : the kind of normalization, currently are alternatively DCV_NORM_L1 and DCV_NORM_L2
//Return :
//	{NULL}
void NormalizeVector(double* vec, int dataLength, int normKind)
{
	int i;
	double sum;

	//Statistic
	sum = 0;
	for(i=0; i<dataLength; i++)
	{
		if(normKind == DCV_NORM_L1)
		{
			sum += vec[i];
		}
		else if(normKind == DCV_NORM_L2)
		{
			sum += vec[i] * vec[i];
		}
		else
		{
			sum = 1;
		}
	}

	//Normalize
	if(sum != 0)
	{
		if(normKind == DCV_NORM_L2)
		{
			sum = sqrt(sum);
		}

		for(i=0; i<dataLength; i++)
		{
			vec[i] /= sum;
		}
	}
}


inline void SwapInt(int* a, int* b)
{
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}


inline void SwapDouble(double* a, double* b)
{
	double tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

int Partition(double a[], int index[], int i,int j)
{
	int x;
	int mid, ptr, sptr;
    mid = (j+i)/2;
    SwapDouble(&a[i],&a[mid]);
	SwapInt(&index[i], &index[mid]);
    ptr = i;
    sptr = i+1;
    for(x = i+1;x<=j;x++)
    {
        if(a[x] < a[ptr])
        {
            SwapDouble(&a[x],&a[sptr]);
			SwapInt(&index[x],&index[sptr]);
            SwapDouble(&a[ptr],&a[sptr]);
			SwapInt(&index[ptr],&index[sptr]);
			ptr++;
			sptr++;
        }
    }
    return ptr;
}

void QuickSort(double obj[], int index[], int p,int q)
{
    if(p<q)
    {
        int par = Partition(obj,index,p,q);
        QuickSort(obj,index,p,par-1);
        QuickSort(obj,index,par+1,q);
    }
}

int KNNPredict(double** trainFeatures, int* trainIndex, int count, double* testFeature, int length, int N)
{
	const int MAX_LABEL = 500;
	int i;
	int labels[MAX_LABEL];
	double* dists;
	int* indexes;
	int maxIdx;
	int maxCount;
	dists = (double*)malloc(sizeof(double) * count);
	indexes = (int*)malloc(sizeof(int) * count);
	for(i=0; i<count; i++)
	{
		dists[i] = LBPDistance(trainFeatures[i], testFeature, length);
		indexes[i] = i;
	}
	
	memset(labels, 0, sizeof(int) * MAX_LABEL);

	QuickSort(dists, indexes, 0, count - 1);
	for(i=0; i<N; i++)
	{
		labels[trainIndex[indexes[i]]] ++;
	}
	maxCount = 0;
	for(i=0; i<MAX_LABEL; i++)
	{
		if(labels[i] > maxCount)
		{
			maxIdx = i;
			maxCount = labels[i];
		}
	}
	free(dists);
	free(indexes);
	return maxIdx;
}

Mat LBPFeatureMatforKMeans(vector<vector<int>> lbpfeatures)
{
	int count = 0;
	for(int i = 0; i < lbpfeatures.size(); i++)
	{
		count += lbpfeatures.at(i).size();
	}
	Mat OneDimLBPMat(count, 1, CV_8UC1);
	
	int counter = 0;
	int startPoint = 0;
	int endPoint = 0;
	for(int i = 0; i < lbpfeatures.size(); i++)
	{
		endPoint = lbpfeatures.at(i).size();
		for(int j = 0; j < lbpfeatures.at(i).size(); j++)
		{	
			OneDimLBPMat.at<uchar>(counter, 0) = lbpfeatures.at(i).at(j);			
			counter++;
		}
		
		startPoint = endPoint + 1;
	}
	return OneDimLBPMat;
}


Mat MakeKMeansCentersInMat(vector<Mat> vecMatCenters)
{
	int counts = 0;
	for(int i = 0; i < vecMatCenters.size(); i++)
	{
		for(int j = 0; j < vecMatCenters.at(i).rows; j++)
		{
			counts++;
		}
	}

	Mat CenterMat(counts, 1, CV_32FC1);

	int counter = 0;
	for(int i = 0; i < vecMatCenters.size(); i++)
	{
		for(int j = 0; j < vecMatCenters.at(i).rows; j++)
		{
			CenterMat.at<float>(counter, 0) = vecMatCenters.at(i).at<float>(j, 0);
			counter++;
		}
	}
	
	return CenterMat;
}


vector<Mat> GetClusterLabelMat(vector<vector<vector<int>>> WholeLBPCollection, Mat centers)
{
	vector<Mat> result;
	float diff = 255.0;
	float label = 0;
	
	for(int fcounter = 0; fcounter < WholeLBPCollection.size(); fcounter++)
	{
		Mat temp(WholeLBPCollection.at(fcounter).size(), WholeLBPCollection.at(fcounter).at(0).size(), CV_32FC1);
		
		for(int row = 0; row < WholeLBPCollection.at(fcounter).size(); row++)
		{

			for(int col = 0; col < WholeLBPCollection.at(fcounter).at(row).size(); col++)
			{
				diff = 255.0;
				for(int ctrCount = 0; ctrCount < centers.rows; ctrCount++)
				{ 
					if(diff > fabs(centers.at<float>(ctrCount,0) - WholeLBPCollection.at(fcounter).at(row).at(col)))
					{
						diff = fabs(centers.at<float>(ctrCount,0) - WholeLBPCollection.at(fcounter).at(row).at(col));
						label = centers.at<float>(ctrCount, 0);
					}

				}
				temp.at<float>(row, col) = label;
			}
		}
		result.push_back(temp);
		temp.release();
	}
	return result;
}


//Make blocks for one image (based on cluster label collection)
//blockRowNum is the block number per Row
//blockColNum is the block number per Col
vector<Mat> makeVectorMatBlockFromClusterLabel(Mat ClusterLabel, int blockRowNum, int blockColNum)
{
	int row = ClusterLabel.rows;
	int col = ClusterLabel.cols;

	int rowLen = row/blockRowNum;
	int colLen = col/blockColNum;

	vector<Mat> result;

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			Mat block(rowLen, colLen, CV_32FC1);
			for(int brow = 0; brow < rowLen; brow++)
			{
				for(int bcol = 0; bcol < colLen; bcol++)
				{
					block.at<float>(brow, bcol) = ClusterLabel.at<float>(i*rowLen + brow, j*colLen + bcol);
				}
			}
			result.push_back(block);
			block.release();
		}
	}
	return result;
}


//Make blocks for one image (based on LBP code collection)
//blockRowNum is the block number per Row
//blockColNum is the block number per Col
vector<Mat> makeVectorMatBlockFromLBPCollection(vector<vector<int>> LBPCollection, int blockRowNum, int blockColNum)
{
	int row = LBPCollection.size();
	int col = LBPCollection.at(0).size();

	int rowLen = row/blockRowNum;
	int colLen = col/blockColNum;

	vector<Mat> result;

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			Mat block(rowLen, colLen, CV_32FC1);

			for(int brow = 0; brow < rowLen; brow++)
			{
				for(int bcol = 0; bcol < colLen; bcol++)
				{
					block.at<float>(brow, bcol) = (float)LBPCollection.at(i*rowLen + brow).at(j*colLen + bcol);
				}
			}
			result.push_back(block);
		}
	}
	return result;
}


//block is the cluster label Mat.
double getMajorityDensity(Mat block, Mat centers, int& majority)
{
	int size = block.rows * block.cols;

	int count = 0;
	int label = -1;
	vector<int> temp(centers.rows);
	temp.assign(centers.rows,0);

	for(int i = 0; i < block.rows; i++)
	{
		for(int j = 0; j < block.cols; j++)
		{
			for(int k = 0; k < centers.rows; k++)
			{
				if((int)block.at<float>(i,j) == (int)centers.at<float>(k,0))
				{
					temp.at(k)++;
				}
			}
		}
	}

	//find the majority cluster label
	int templabel = temp.at(0);
	int idx = 0;
	for(int i = 0; i < temp.size(); i++)
	{
		if(templabel < temp.at(i))
		{
			templabel = temp.at(i);
			idx = i;
		}
	}
	majority = idx;
	double result = (double)templabel/(double)size;
	
	return result;
}

Mat MakeBlockLabelforImage(vector<Mat> block, int blockRowNum, int blockColNum, Mat centers)
{
	int label;
	Mat BlockLabelImage(blockRowNum, blockColNum, CV_32FC1);

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			getMajorityDensity(block.at(i * blockRowNum + j), centers, label);
			BlockLabelImage.at<float>(i, j) = centers.at<float>(label, 0);
		}
	}

	return BlockLabelImage;
}


Mat MakeMatforSVMInput(vector<Mat> blockLabel)
{
	int cols = blockLabel.at(0).rows * blockLabel.at(0).cols;

	Mat SVMInput(blockLabel.size(), cols, CV_32FC1);

	for(int i = 0; i < blockLabel.size(); i++)
	{
		Mat rowMat(1, cols, CV_32FC1);

		for(int row = 0; row < blockLabel.at(i).rows; row++)
		{
			for(int col = 0; col < blockLabel.at(i).cols; col++)
			{
				rowMat.at<float>(0, row * blockLabel.at(i).rows + col) = blockLabel.at(i).at<float>(row, col);
			}
		}
		rowMat.copyTo(SVMInput.rowRange(i, i+1));
		rowMat.release();
	}

	return SVMInput;
}

Mat MakeMatforLabelFromVector(vector<int> vlabel)
{
	int size = vlabel.size();

	Mat labelmat(size, 1, CV_32FC1);
	for(int i = 0; i < size; i++)
	{
		labelmat.at<float>(i,0) = vlabel.at(i);
	}
	return labelmat;
}

//Gather the collection of sub-images from one source image.
vector<vector<WinInfo*>> _extractSubImg(Mat source, Size initWinSize, double factor, int xStep, int yStep, int layer, Point position = Point(0,0))
{

	vector<vector<WinInfo*>> subimages;

	int sourceWidth = source.cols;
	int sourceHeight = source.rows;
	int layercount = 0;

	while(layercount < layer)
	{
		cout << layercount << endl;
		vector<WinInfo*> windows;

		for(int h = position.y; h + initWinSize.height <= int((double)sourceHeight * pow(factor, double(layercount))); h += yStep)
		{
			for(int w = position.x; w + initWinSize.width <= int((double)sourceWidth * pow(factor, double(layercount))); w += xStep)
			{
				WinInfo* window = new WinInfo(initWinSize, position, source, layercount, factor);
				
				if((*window)._checkWindow())
				{
					(*window)._updateLayer(layercount);
					(*window)._updateLocation(Point(w, h));
					Mat temp_source = (*window)._updateSource();
					(*window)._extractWindow(temp_source);
					windows.push_back(window);
					
					temp_source.release();
					//(*window).~WinInfo();
				}
			}
		}
		subimages.push_back(windows);
		windows.clear();
		layercount++;
	}

	return subimages;
}


Mat _preprocessingOneSubImg(Mat subImg, Mat TrainWholeCenters, LBPModel lbpModel)
{
	Mat subImg_result;
	const int LBP_N_COL = 10;
	const int LBP_N_ROW = 10;
	int ImgRowBlock = 20;
	int ImgColBlock = 20;

	Mat* lbpMap;
	Mat grayImg;
	Mat grayImg32f;
	Mat iiGrayImg32f;
	//LBPModel lbpModel;
	int ptr = 0;
	double* testFeatures;

	vector<vector<int>> LBPTestCollection;
	vector<vector<vector<int>>> WholeLBPTestCollection;

	//resize
	subImg_result = subImg;
	resize(subImg_result, subImg_result, Size(100, 100), 0.0, 0.0, INTER_CUBIC);
	
	//canny edge detection
	Mat CannyEdgeImg = GetCannyEdgeMat(subImg_result);
	//Mat CannyEdgeImg = GetLaplacianMat(subImg_result, -1, 1);


	cvtColor(CannyEdgeImg, grayImg, CV_LOAD_IMAGE_GRAYSCALE);
	
	//prepare for LBP
	grayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);
	iiGrayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);		
	grayImg.convertTo(grayImg32f, IPL_DEPTH_32F);
	
	//LBP
	lbpMap = GetLBPMap(grayImg, &lbpModel, LBPTestCollection);
	testFeatures = ExtractLBPFeature(lbpMap, LBP_N_ROW, LBP_N_COL, &lbpModel);
	WholeLBPTestCollection.push_back(LBPTestCollection);
	
	LBPTestCollection.clear();
	grayImg.release();
	grayImg32f.release();
	iiGrayImg32f.release();
	(*lbpMap).release();
	//Img.release();
	CannyEdgeImg.release();

	//centers
	vector<Mat> labelTestCluster = GetClusterLabelMat(WholeLBPTestCollection, TrainWholeCenters);
	//vector<vector<Mat>> blockWholeTestImages;
	vector<Mat> TestblockLabelMat;

	for(int i = 0; i < labelTestCluster.size(); i++)
	{
		//blockWholeTestImages.push_back(makeVectorMatBlockFromClusterLabel(labelTestCluster.at(i), ImgRowBlock, ImgColBlock));
		TestblockLabelMat.push_back(MakeBlockLabelforImage(makeVectorMatBlockFromClusterLabel(labelTestCluster.at(i), ImgRowBlock, ImgColBlock), ImgRowBlock, ImgColBlock, TrainWholeCenters));
	}

	Mat svmTestData = MakeMatforSVMInput(TestblockLabelMat);
	
	TestblockLabelMat.clear();
	labelTestCluster.clear();
	free(testFeatures);

	return svmTestData;
}

int main(int argc, char* argv[])
{
	int ImgRowBlock = 20;
	int ImgColBlock = 20;

	const int LBP_N_COL = 10;
	const int LBP_N_ROW = 10;
	int ptr;
	int i, j;

	Mat* lbpMap;

	Mat grayImg;
	Mat grayImg32f;
	Mat iiGrayImg32f;

	LBPModel lbpModel;

	ClassInfo* trainInfo;
	ClassInfo* testInfo;

	double** trainFeatures;
	double** testFeatures;

	int* trainIndex;
	int* testIndex;
	int error = 0;

	argv[1] = "C:/Course/C plusplus test/test1/samples";
	//argv[2] = "C:/Course/C plusplus test/test1/testsamples";
	argv[2] = "C:/Course/C plusplus test/hogtest/imgs";
	//argv[2] = "C:/Course/C plusplus test/test1/testset";
	
	trainInfo = TraversClassFolder(argv[1]);
	testInfo = TraversClassFolder(argv[2]);

	trainFeatures = (double**)malloc(sizeof(double*) * trainInfo->fileCount);
	testFeatures = (double**)malloc(sizeof(double*) * testInfo->fileCount);
	trainIndex = (int*)malloc(sizeof(int) * trainInfo->fileCount);
	testIndex = (int*)malloc(sizeof(int) * testInfo ->fileCount);


	//Initialize LBP
	//lbpModel.type = lbpModel.DCV_LBPTYPE_TERNARY;
	lbpModel.type = lbpModel.DCV_LBPTYPE_BINARY;
	lbpModel.scaleRatio = 0.04;
	lbpModel.ternaryRange = 5;
	lbpModel.nNeighbor = 8;
	lbpModel.radius = 1;
	lbpModel.uniformed = 0;
	InitializeLBPModel(&lbpModel);
	
	///For cluster (K-Means)
	Mat labels;
	Mat centers;
	int K = 10;
	///

	vector<int> ClassSeparateCounts;
	vector<int> ClassTestSeparateCounts;

	ptr = 0;
	vector<vector<int>> LBPTrainCollection;
	vector<vector<vector<int>>> WholeLBPTrainCollection;
	vector<vector<vector<int>>> WholeLBPTestCollection;
	vector<vector<int>> LBPTestCollection;
	vector<int> TempLBPCollection;
	vector<Mat> vecMatCenters;
	int counts = 0;

	/////set ExtractFeatureTime for getting beginning time of extracting features.
	clock_t ExtractFeatureTime;
	ExtractFeatureTime = clock();
	cout << "Extracting Training Feature" << endl;

	for(i = 0; i < trainInfo->classCount; i++)
	{
		
		for(j = 0; j < trainInfo->fileCounts[i]; j++)
		{
			cout << "Class: " << i << "  Sample: " << j << endl;
			//grayImg = imread(trainInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_GRAYSCALE);
			Mat Img = imread(trainInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_COLOR);
			Mat dst;
			resize(Img, dst, Size(100, 100), 0.0, 0.0, INTER_CUBIC);
			
			Mat CannyEdgeImg = GetCannyEdgeMat(dst);
			//Mat CannyEdgeImg = GetLaplacianMat(dst, -1, 1);

			cout <<  trainInfo->fileName[i][j].c_str() << endl;
			
			string cannyfile = "Canny\\";
			char cannytempi[10];
			itoa(i, cannytempi, 10);
			char cannytempj[10];
			itoa(j, cannytempj, 10);

			string cannyEdgeFileName = string(trainInfo->fileName[i][j].c_str());
			size_t found = cannyEdgeFileName.find_last_of("\\");
			string OutputCannyFileName = cannyEdgeFileName.substr(found+1, cannyEdgeFileName.length());
			
			//imwrite(cannyfile + OutputCannyFileName, CannyEdgeImg);
			//imwrite(cannyfile + string(trainInfo->fileName[i][j].c_str()), CannyEdgeImg);

			cvtColor(CannyEdgeImg, grayImg, CV_LOAD_IMAGE_GRAYSCALE);

			grayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);
			iiGrayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);

			grayImg.convertTo(grayImg32f, IPL_DEPTH_32F);
			
			Mat centers;		
			lbpMap = GetLBPMap(grayImg32f, &lbpModel, LBPTrainCollection);
			
			/////////K-Means Part for per Image/////////
			//the kmeansMat is used as input for KMeans clustering.
			Mat kmeansMat = LBPFeatureMatforKMeans(LBPTrainCollection);
			kmeansMat.convertTo(kmeansMat, CV_32FC1);
			kmeansMat.copyTo(labels);
			labels.setTo(Scalar(0),noArray());
			kmeans(kmeansMat, K, labels, TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 10, 0.1), 3, cv::KMEANS_PP_CENTERS, centers);
			vecMatCenters.push_back(centers);
			////////K-Means Part END////////////////////

			trainFeatures[ptr] = ExtractLBPFeature(lbpMap, LBP_N_ROW, LBP_N_COL, &lbpModel);
			trainIndex[ptr] = i;
			ptr++;
			grayImg.release();
			grayImg32f.release();
			iiGrayImg32f.release();
			(*lbpMap).release();
			
			Img.release();
			CannyEdgeImg.release();
			//dst.release();

			WholeLBPTrainCollection.push_back(LBPTrainCollection);
			LBPTrainCollection.clear();

			counts = i;
			ClassSeparateCounts.push_back(counts);////////
		}	
	}

	Mat TrainImagesCenters = MakeKMeansCentersInMat(vecMatCenters);
	Mat TrainImagesLabels;
	Mat TrainWholeCenters;

	TrainImagesCenters.copyTo(TrainImagesLabels);
	TrainImagesLabels.setTo(Scalar(0),noArray());
	kmeans(TrainImagesCenters, K, TrainImagesLabels, TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 10, 0.1), 3, cv::KMEANS_PP_CENTERS, TrainWholeCenters);
	
	vector<Mat> labelCluster = GetClusterLabelMat(WholeLBPTrainCollection, TrainWholeCenters);
	vector<vector<Mat>> blockWholeImages;
	vector<Mat> blockLabelMat;
	
	for(int i = 0; i < labelCluster.size(); i++)
	{
		blockWholeImages.push_back(makeVectorMatBlockFromClusterLabel(labelCluster.at(i), ImgRowBlock, ImgColBlock));
		blockLabelMat.push_back(MakeBlockLabelforImage(makeVectorMatBlockFromClusterLabel(labelCluster.at(i), ImgRowBlock, ImgColBlock), ImgRowBlock, ImgColBlock, TrainWholeCenters));
	}

	//end of extracting features, and show the duration
	clock_t ExtractFeatureTimeEnd = clock();
	double ExtractFeatureDuration = (ExtractFeatureTimeEnd - ExtractFeatureTime)/(double)CLOCKS_PER_SEC;
	cout << "ExtractFeatureDuration: " << ExtractFeatureDuration << endl;

	Mat svmTrainData = MakeMatforSVMInput(blockLabelMat);
	Mat svmTrainDataLabel = MakeMatforLabelFromVector(ClassSeparateCounts);


	//////////////////////////////////////////////////////////////////////////////
	////////////////////////////SVMs//////////////////////////////////////////////
	
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.gamma = 0.00002;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	CvParamGrid CvParamGrid_C(0, 1, 0);
	CvParamGrid CvParamGrid_Gamma;

    // Train the SVM
    CvSVM SVM;
	
	clock_t TrainingTime = clock();
	//SVM.train(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params);
	/*SVM.train_auto(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
		, CvSVM::get_default_grid(CvSVM::GAMMA), CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
		, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);*/
	SVM.train_auto(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
		, CvParamGrid_C, CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
		, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);
	clock_t TrainingTimeEnd = clock();
	double TrainingDuration = (TrainingTimeEnd - TrainingTime)/(double)CLOCKS_PER_SEC;
	
	cout << "TrainingDuration: " << TrainingDuration << endl;
	cout << endl;

	cout << "Extracting Test Features" << endl;
	ptr = 0;
	int testCount = 0;
	clock_t ExtractTestFeatureTime = clock();

	//////////////////////////////new part////////////////////////////////////
	//new load images//
	//testInfo

	
	for(i = 0; i < testInfo->classCount; i++)
	{
		
		for(j = 0; j < testInfo->fileCounts[i]; j++)
		{
			Mat source1 = imread(testInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_COLOR);
			
			int rHeight = source1.rows;
			int rWidth = source1.cols;

			while(true)
			{
				if(rHeight > 768 && rWidth > 1024)
				{
					rHeight = int(0.8 * double(rHeight));
					rWidth = int(0.8 * double(rWidth));
				}
				else
				{
					resize(source1, source1, Size(rWidth, rHeight));
					break;
				}
			}

			//resize(source1, source1, Size(1024, 768));
			
			Size initWinSize = Size(50, 50);
			
			double factor = 0.5;
			Point position = Point(0,0);
			int xStep = 25;
			int yStep = 25;
			int sourceWidth = source1.cols;
			int sourceHeight = source1.rows;
			int layer = 5;

			vector<vector<WinInfo*>> collection = _extractSubImg(source1, initWinSize, factor, xStep, yStep, layer, position);
			Mat temp = source1;

			for(int level = 0; level < collection.size(); level++)
			{

				for(int subimgcount = 0; subimgcount < collection.at(level).size(); subimgcount++)
				{

					Mat SVMpredictWin = _preprocessingOneSubImg(collection.at(level).at(subimgcount)->win, TrainWholeCenters, lbpModel);
					float response = SVM.predict(SVMpredictWin, true);

					if(response > 0)
					{
						cout << "predict: " << SVM.predict(SVMpredictWin, true) << endl;
						temp = collection.at(level).at(subimgcount)->_drawRect(collection.at(level).at(subimgcount)->_getRect(), temp);
					}
					SVMpredictWin.release();
					collection.at(level).at(subimgcount)->~WinInfo();
				}
			}

			//save
			char tcount[10];
			itoa(j, tcount, 10);
			
			string savename = "C:/Course/C plusplus test/DetectionTest1/save/" + string(tcount) + ".jpg";
			cout << savename << " done  " << imwrite(savename, temp) << endl;

			temp.release();
			collection.clear();
			source1.release();
		}
	}

	return 0;
}
