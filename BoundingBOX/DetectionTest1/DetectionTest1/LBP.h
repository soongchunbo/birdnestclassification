#include "StdAfx.h"

#ifndef DCV_BG_PROJECT_RECOGNITION_LBP
#define DCV_BG_PROJECT_RECOGNITION_LBP

#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>


using namespace std;
using namespace cv;

struct CenterPoint
{
public:
	Point2i p;
	int radius;

};


//vector<vector<int>> LBPcollection;

void getLBP(Mat inputMat, Mat outputMat, int radius);

//default radius is equal to 1.
void getCSLBP(Mat inputMat, Mat outputMat);

#define DCV_MAT_ELEM( mat, elemtype, row, col )	\
	(*(elemtype*)((mat).data.ptr + (size_t)(mat).step*(row) + (sizeof(elemtype))*(col)))

typedef struct
{
	//Type of LBP
	typedef enum
	{
		DCV_LBPTYPE_BINARY, DCV_LBPTYPE_TERNARY, DCV_LBPTYPE_SCALEINVARIANT_TERNARY,
	}LBP_TYPE;
	LBP_TYPE type;

	//Control if LBP is uniformed
	int uniformed;
	int* mappingTable;

	//Standard LBP coefficients
	int nNeighbor;
	double radius;
	int codeLength;

	//LTP parameters
	int ternaryRange;
	double scaleRatio;

}LBPModel;




void InitializeLBPModel(LBPModel* lbpModel);
void ReleaseLBPModel(LBPModel* lbpModel);

Mat* GetLBPMap(Mat srcImg32f, LBPModel* lbpModel, vector<vector<int>>& LBPcollection);
double* ExtractLBPFeature(Mat* lbpMat32f, int nRow, int nCol, LBPModel* lbpModel);
double LBPDistance(double* feature1, double* feature2, int length);

#endif