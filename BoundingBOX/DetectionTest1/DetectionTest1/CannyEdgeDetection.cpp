#include "StdAfx.h"
#include "CannyEdgeDetection.h"

Mat GetCannyEdgeMat(Mat src)
{
	Mat canny_img_mid(src.rows, src.cols, src.type());
	Mat canny_img_result(src.rows, src.cols, src.type());
	bitwise_not(src, canny_img_mid);
	
	Canny(src, canny_img_mid, LOWTHRESHOLD, UPPERTHRESHOLD, 3);
	
	cvZero(&(IplImage)canny_img_result);
	cvCopy(&(IplImage)src, &(IplImage)canny_img_result, &(IplImage)canny_img_mid);
	canny_img_mid.release();
	return canny_img_result;
}


Mat GetLaplacianMat(Mat src, int depth, int ksize)
{
	Mat dst(src.rows, src.cols, src.type());
	
	Laplacian(src, dst, depth, ksize, 1.0, 0.0, BORDER_DEFAULT);
	
	

	return dst;
}