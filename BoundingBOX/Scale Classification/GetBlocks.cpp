#include "stdafx.h"
#include "GetBlocks.h"

Mat _getOneBlock(Mat img, Point tl, Point br)
{
	Rect rect(tl, br);
	Mat block = img(rect);
	return block;
}

vector<Mat> _getCollectionBlocks(Mat img, int bwidth, int bheight, int colstep, int rowstep)
{
	vector<Mat> collection;
	
	int rows = img.rows;
	int cols = img.cols;
	Point tl, br;

	if(colstep == 0)
	{
		colstep = bwidth;
	}
	
	if(rowstep == 0)
	{
		rowstep = bheight;
	}

	for(int i = 0; i < (rows - rowstep); i += rowstep)
	{
		for(int j = 0; j < (cols - colstep); j += colstep)
		{
			tl = Point(j, i);
			br = Point(j+bwidth,i+bheight);

			collection.push_back(_getOneBlock(img, tl, br));
		}
	}

	return collection;
}