#include <iostream>
#include <vector>
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\flann\flann.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\ml\ml.hpp>
#include <opencv2\objdetect\objdetect.hpp>
using namespace std;
using namespace cv;


Mat _getOneBlock(Mat img, Point tl, Point br);

vector<Mat> _getCollectionBlocks(Mat img, int bwidth, int blength, int colstep, int rowstep);
