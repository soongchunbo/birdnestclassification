#include <iostream>
#include <vector>
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\flann\flann.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\ml\ml.hpp>
#include <opencv2\objdetect\objdetect.hpp>
using namespace std;
using namespace cv;

struct myParamforMeanAndDev{
public:
	Scalar mean;
	Scalar dev;
};

myParamforMeanAndDev _getMeanAndDev(Mat inputMat);

Mat _makeBlocksToMeanRow(vector<Mat> blocks);

Mat _makeBlocksToDevRow(vector<Mat> blocks);

Mat _combineRowsIntoMat(vector<Mat> rows);

Mat _combineTwoMatIntoOneMat(Mat mean, Mat dev);

Mat _makeFloatMat(Mat input);