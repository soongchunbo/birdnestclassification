#include "stdafx.h"
#include "Classification.h"

Mat _SVMClassification(Mat input, Mat label)
{
	Mat output;

	//////////////////////////////////////////////////////////////////////////////
	////////////////////////////SVMs//////////////////////////////////////////////
	
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.gamma = 0.01;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	CvParamGrid CvParamGrid_C(0, 1, 0);
	CvParamGrid CvParamGrid_Gamma;

    // Train the SVM
    CvSVM SVM;

	cout << "input size: " << input.rows << endl;
	cout << "label size: " << label.rows << endl;

	SVM.train_auto(input, label, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
		, CvParamGrid_C, CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
		, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);

	SVM.predict(input, output);

	/*for(int i = 0; i < output.rows; i++)
	{
		cout << "predict: " << output.at<float>(i,0) << "  ground truth: " << label.at<float>(i,0) << endl;
	}*/

	_makeComparison(output, label);

	return output;
}

void _makeComparison(Mat predict, Mat groundTruth)
{
	if(predict.rows != groundTruth.rows)
		return;

	int size = predict.rows;
	int correctClassified = 0;
	int wrongClassified = 0;

	for(int i = 0; i < size; i++)
	{
		if(predict.at<float>(i,0) == groundTruth.at<float>(i,0))
			correctClassified++;
		else
			wrongClassified++;
	}

	double accuracy = (correctClassified*1.0)/(size*1.0);

	cout << "Total Result: Correct: " << correctClassified << " Wrong: " 
		<< wrongClassified << " Accuracy:" << accuracy << endl;

}