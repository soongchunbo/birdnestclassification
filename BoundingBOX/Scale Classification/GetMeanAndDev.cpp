#include "stdafx.h"
#include "GetBlocks.h"
#include "GetMeanAndDev.h"

myParamforMeanAndDev _getMeanAndDev(Mat inputMat)
{
	myParamforMeanAndDev result;

	meanStdDev(inputMat, result.mean, result.dev);

	//cout << "mean: " << result.mean << endl;
	//cout << "stdDev: " << result.dev << endl;
	return result;
}

Mat _makeBlocksToMeanRow(vector<Mat> blocks)
{
	int size = blocks.size();
	Mat MeanRow(1, size, CV_32FC3);

	cout << "size: " << size << endl;
	
	for(int i = 0; i < MeanRow.cols; i++)
	{
		//row.at<Vec3f>(0,i)[0] = 1;
		MeanRow.at<Vec3f>(0,i)[0] = _getMeanAndDev(blocks.at(i)).mean[0];
		MeanRow.at<Vec3f>(0,i)[1] = _getMeanAndDev(blocks.at(i)).mean[1];
		MeanRow.at<Vec3f>(0,i)[2] = _getMeanAndDev(blocks.at(i)).mean[2];
	}

	return MeanRow;
}

Mat _makeBlocksToDevRow(vector<Mat> blocks)
{
	int size = blocks.size();
	Mat DevRow(1, size, CV_32FC3);

	cout << "size: " << size << endl;
	
	for(int i = 0; i < DevRow.cols; i++)
	{
		//row.at<Vec3f>(0,i)[0] = 1;
		DevRow.at<Vec3f>(0,i)[0] = _getMeanAndDev(blocks.at(i)).dev[0];
		DevRow.at<Vec3f>(0,i)[1] = _getMeanAndDev(blocks.at(i)).dev[1];
		DevRow.at<Vec3f>(0,i)[2] = _getMeanAndDev(blocks.at(i)).dev[2];
	}

	return DevRow;
}

Mat _combineRowsIntoMat(vector<Mat> rows)
{
	int col = rows.at(0).cols;
	int row = rows.size();
	Mat output(row, col, CV_32FC3);

	for(int i = 0; i < row; i++)
	{
		//output.row(i).copyTo(rows.at(i));
		rows.at(i).copyTo(output.row(i));
	}

	return output;
}

Mat _combineTwoMatIntoOneMat(Mat mean, Mat dev)
{
	Mat output(mean.rows, mean.cols+dev.cols, CV_32FC3);

	int idx = 0;
	for(int i = 0; i < mean.cols; i++)
	{
		//output.col(idx).copyTo(mean.col(i));
		mean.col(i).copyTo(output.col(idx));
		idx++;
		//output.col(idx).copyTo(dev.col(i));
		dev.col(i).copyTo(output.col(idx));
		idx++;
	}

	/*for(int i = 0; i < output.rows; i++)
	{
		for(int j = 0; j < output.cols; j++)
		{
			cout << output.at<Vec3f>(i,j)[0] << endl;
		}
	}*/

	return output;
}

Mat _makeFloatMat(Mat input)
{
	int cols = input.cols * 3;
	int rows = input.rows;
	Mat floatmat(rows, cols, CV_32FC1);

	for(int i = 0; i < rows; i++)
	{
		int k = 0;
		for(int j = 0; j < input.cols; j++)
		{
			//cout << "k " << k << endl; 
			floatmat.at<float>(i,k) = input.at<Vec3f>(i,j)[0];
			k++;
			floatmat.at<float>(i,k) = input.at<Vec3f>(i,j)[1];
			k++;
			floatmat.at<float>(i,k) = input.at<Vec3f>(i,j)[2];
			k++;
		}
	}

	return floatmat;
}