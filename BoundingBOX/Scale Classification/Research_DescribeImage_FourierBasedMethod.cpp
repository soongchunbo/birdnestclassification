// Research_DescribeImage_FourierBasedMethod.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\flann\flann.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\ml\ml.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <iostream>
#include <vector>

#include "IOOperation.h"
#include "GetBlocks.h"
#include "GetMeanAndDev.h"
#include "Classification.h"

using namespace std;
using namespace cv;

Mat _getLaplacian(Mat src)
{
	Mat src_gray, dst;
	int kernel_size = 3;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;
	//char* window_name = "Laplace Demo";

	int c;

	/// Remove noise by blurring with a Gaussian filter
	GaussianBlur( src, src, Size(3,3), 0, 0, BORDER_DEFAULT );

	cvtColor( src, src_gray, CV_BGR2GRAY );

	Mat abs_dst;

	Laplacian( src_gray, dst, ddepth, kernel_size, scale, delta, BORDER_DEFAULT );
	convertScaleAbs( dst, abs_dst );

	/// Show what you got
	imshow("test", abs_dst );

	waitKey(0);

	return abs_dst;
}

Mat _getDFT(Mat image)
{
	Mat I = image;

	Mat padded;                            //expand input image to optimal size
    int m = getOptimalDFTSize( I.rows );
    int n = getOptimalDFTSize( I.cols ); // on the border add zero values
    
	copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));

    Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
    Mat complexI;
    merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

    dft(complexI, complexI);            // this way the result may fit in the source matrix

    // compute the magnitude and switch to logarithmic scale
    // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
    split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
    magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
    Mat magI = planes[0];

    magI += Scalar::all(1);                    // switch to logarithmic scale
    log(magI, magI);

    // crop the spectrum, if it has an odd number of rows or columns
    magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

    // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = magI.cols/2;
    int cy = magI.rows/2;

    Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
    Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
    Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

    Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);

    q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
    q2.copyTo(q1);
    tmp.copyTo(q2);

    normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
                                            // viewable image form (float between values 0 and 1).

    imshow("Input Image"       , I   );    // Show the result
    imshow("spectrum magnitude", magI);
    waitKey();

	return I;
}

int main(int argc, char* argv[])
{
	ClassInfo* trainInfo;
	ClassInfo* testInfo;

	argv[1] = "C:/Course/C plusplus test/dataSet/DifferentScales";
	//argv[2] = "C:/Course/C plusplus test/test1/testsamples";
	argv[2] = "C:/Course/C plusplus test/dataSet/DifferentScales";

	trainInfo = TraversClassFolder(argv[1]);
	testInfo = TraversClassFolder(argv[2]);

	double** trainFeatures;
	double** testFeatures;
	int* trainIndex;
	int* testIndex;
	int error = 0;

	trainFeatures = (double**)malloc(sizeof(double*) * trainInfo->fileCount);
	testFeatures = (double**)malloc(sizeof(double*) * testInfo->fileCount);
	trainIndex = (int*)malloc(sizeof(int) * trainInfo->fileCount);
	testIndex = (int*)malloc(sizeof(int) * testInfo ->fileCount);

	vector<Mat> collection;
	vector<Mat> rowscollection;
	vector<Mat> rowdevcollection;
	//cout << trainInfo->fileCount << endl;

	Mat label(trainInfo->fileCount,1,CV_32FC1);
	int labelIdx = 0;

	for(int i = 0; i < trainInfo->classCount; i++)
	{
		
		for(int j = 0; j < trainInfo->fileCounts[i]; j++)
		{
			Mat img = imread(trainInfo->fileName[i][j].c_str(), 1);
			resize(img, img, Size(800, 600), 0.0, 0.0, INTER_CUBIC);

			//cvtColor(img, img, CV_LOAD_IMAGE_GRAYSCALE);
			//_getLaplacian(img);

			collection = _getCollectionBlocks(img,10,10,0,0);
			rowscollection.push_back(_makeBlocksToMeanRow(collection));
			rowdevcollection.push_back(_makeBlocksToDevRow(collection));
			//_getDFT(img);

			label.at<float>(labelIdx,0) = i;
			labelIdx++;
			cout << trainInfo->fileName[i][j].c_str() << " is done." << endl;
		}
	}

	Mat inputForClassifier = _combineRowsIntoMat(rowscollection);
	Mat inputDevClassifier = _combineRowsIntoMat(rowdevcollection); 
	//cout << inputForClassifier.rows << endl;
	Mat input = _combineTwoMatIntoOneMat(inputForClassifier, inputDevClassifier);
	
	
	_SVMClassification(_makeFloatMat(input), label);

	return 0;
}

