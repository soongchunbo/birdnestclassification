#include "StdAfx.h"
#include "TestingProcedure.h"

void TestCheckCenters(Mat centers)
{
	for(int i = 0; i < centers.rows; i++)
	{
		cout << (int)centers.at<float>(i,0) << endl;
	}
}

void TestBlockLabelMat(Mat blockLabelMat)
{
	for(int i = 0; i < blockLabelMat.rows; i++)
	{
		for(int j = 0; j < blockLabelMat.cols; j++)
		{
			cout << (int)blockLabelMat.at<float>(i, j) << endl;
		}
	}
}

void TestOneDimVector(vector<int> v)
{
	for(int i = 0; i < v.size(); i++)
	{
		cout << "one-Dimension vector test: " << v.at(i) << endl;
	}
}

void TestCompareSVMResponse(Mat actualLabel, Mat response, TrainParameters& tparam, ClassInfo* testInfo)
{
	cout << "actualLabel rows: " << actualLabel.rows << " actualLabel cols: " << actualLabel.cols << endl;
	cout << "response rows: " << response.rows << " response cols: " << response.cols << endl;
	int rows = actualLabel.rows;
	int cols = actualLabel.cols;
	int errors = 0;

	if((actualLabel.rows != response.rows) && (actualLabel.cols != response.cols))
	{
		cout << "ERROR!" << endl;
		return;
	}

	int pError = 0;
	int nError = 0;

	int CountPositiveMisclassification = 0;
	int CountPositiveCorrectClassification = 0;
	int CountNegativeMisclassification = 0;
	int CountNegativeCorrectClassification = 0;

	for(int i = 0; i < rows; i++)
	{
		if(actualLabel.at<float>(i,0) != response.at<float>(i,0))
		{
			cout << "Index: " << i << " Actual Class: " << actualLabel.at<float>(i,0) 
				<< " ; Predict Class: " << response.at<float>(i,0) << " Wrong! "<< endl;

			if(actualLabel.at<float>(i,0) == 0)
			{
				CountPositiveMisclassification++;
			}
			else
			{
				CountNegativeMisclassification++;
			}

			int TotalMisClassfiedIndex = 0;
			for(int testClasses = 0; testClasses < testInfo->classCount; testClasses++)
			{
				for(int testClassFiles = 0; testClassFiles < testInfo->fileCounts[testClasses]; testClassFiles++)
				{
					TotalMisClassfiedIndex++;
					if(i == TotalMisClassfiedIndex)
					{
						cout << "MISCLASSIFICATION: " << testInfo->fileName.at(testClasses).at(testClassFiles) << endl;
					}
				}
			}

			errors++;
		}
		else
		{
			cout << "Index: " << i << " Actual Class: " << actualLabel.at<float>(i,0) 
				<< " ; Predict Class: " << response.at<float>(i,0) << " Right! "<< endl;

			if(actualLabel.at<float>(i,0) == 0)
			{
				CountPositiveCorrectClassification++;
			}
			else
			{
				CountNegativeCorrectClassification++;
			}
		}
	}

	cout << "Error rate: " << (double)errors/(double)rows << endl;

	cout << "True Positive: " << CountPositiveCorrectClassification << endl;
	cout << "False Positive: " << CountNegativeMisclassification << endl;
	cout << "True Negative: " << CountNegativeCorrectClassification << endl;
	cout << "False Negative: " << CountPositiveMisclassification << endl;

	tparam.ErrorNum = errors;
	tparam.Accuracy = (1.0 - ((double)errors/(double)rows));

	TestWriteResultToFile(tparam);

}

void TestWriteResultToFile(TrainParameters param)
{
	FILE* fp = fopen("result.txt", "a+");


	int resized = param.Resized;
	double lowerbound = param.CannyLowerBound;
	double upperbound = param.CannyUpperBound;
	int innerK = param.innerK;
	int outerK = param.outerK;
	int rowBlockNum = param.RowBlockNum;
	int colBlockNum = param.ColBlockNum;

	int errors = param.ErrorNum;
	double accuracy = param.Accuracy;

	int svmtype = param.params.svm_type;
	int kerneltype = param.params.kernel_type;
	double svmgamma = param.params.gamma;

	char temp_resized[10];
	itoa(resized, temp_resized,10);
	string title_resized = "Resized: " + string(temp_resized) + "\n";
	fputs(title_resized.c_str(), fp);

	ostringstream strs1;
	strs1 << lowerbound;
	string temp_lb = strs1.str();
	string title_lb = "Canny Edge Detection Lower Bound: " + temp_lb + "\n";
	fputs(title_lb.c_str(), fp);

	ostringstream strs2;
	strs2 << upperbound;
	string temp_ub = strs2.str();
	string title_ub = "Canny Edge Detection Upper Bound: " + temp_ub + "\n";
	fputs(title_ub.c_str(), fp);

	char temp_svmtype[10];
	itoa(svmtype, temp_svmtype, 10);
	string title_svmtype = "SVM Type: " + string(temp_svmtype) + "\n";
	fputs(title_svmtype.c_str(), fp);

	char temp_svmkernel[10];
	itoa(kerneltype, temp_svmkernel, 10);
	string title_svmkernel = "SVM Kernel: " + string(temp_svmkernel) + "\n";
	fputs(title_svmkernel.c_str(), fp);

	ostringstream gamma_strs;
	gamma_strs << svmgamma;
	string gammastr = gamma_strs.str();
	string title_gamma = "SVM Gamma: " + gammastr + "\n";
	fputs(title_gamma.c_str(), fp);

	char temp_errors[10];
	itoa(errors, temp_errors, 10);
	string title_err = "Error Num: " + string(temp_errors) + "\n";
	fputs(title_err.c_str(), fp);

	ostringstream strs_accuracy;
	strs_accuracy << accuracy;
	string temp_accuracy = strs_accuracy.str();
	string title_acc = "Accuracy: " + temp_accuracy + "\n" + "\n" + "\n";
	fputs(title_acc.c_str(), fp);

	fclose(fp);
}