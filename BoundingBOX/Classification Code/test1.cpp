// test1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\nonfree\features2d.hpp>
#include <opencv2\ml\ml.hpp>
#include <iostream>
#include <ctime>
#include "LBP.h"
#include "IOOperation.h"
#include "voidDivideImageIntoBlocks.h"
#include "CannyEdgeDetection.h"

////test procedure///////
#include "TestingProcedure.h"
////test procedure end///

//#include "IlluminationInvariance.h"

#define DCV_NORM_L1 0
#define DCV_NORM_L2 1

using namespace std;
using namespace cv;
using namespace cv::gpu;


//Funcation Abstract :
//	The EUCLIDEAN distance of two vectors
//Params :
//	{vec1 (in)} : the 1st vector
//	{vec2 (in)} : the 2nd vector
//	{dataLength (in)} : the length of the vector
//Return :
//	{double} : the distance value
double VectorDistance(double* vec1, double* vec2, int dataLength)
{
	int i;
	double rtnVal = 0;
	for(i=0;i<dataLength;i++)
	{
		rtnVal += (vec1[i] - vec2[i]) * (vec1[i] - vec2[i]);
	}
	rtnVal = sqrt(rtnVal);

	return rtnVal;
}

//Funcation Abstract :
//	The normalization procedure of a vector
//Params :
//	{vec (in/out)} : the data vector
//	{dataLength (in)} : the length of data in vector
//	{normKind (in)} : the kind of normalization, currently are alternatively DCV_NORM_L1 and DCV_NORM_L2
//Return :
//	{NULL}
void NormalizeVector(double* vec, int dataLength, int normKind)
{
	int i;
	double sum;

	//Statistic
	sum = 0;
	for(i=0; i<dataLength; i++)
	{
		if(normKind == DCV_NORM_L1)
		{
			sum += vec[i];
		}
		else if(normKind == DCV_NORM_L2)
		{
			sum += vec[i] * vec[i];
		}
		else
		{
			sum = 1;
		}
	}

	//Normalize
	if(sum != 0)
	{
		if(normKind == DCV_NORM_L2)
		{
			sum = sqrt(sum);
		}

		for(i=0; i<dataLength; i++)
		{
			vec[i] /= sum;
		}
	}
}


inline void SwapInt(int* a, int* b)
{
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}


inline void SwapDouble(double* a, double* b)
{
	double tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

int Partition(double a[], int index[], int i,int j)
{
	int x;
	int mid, ptr, sptr;
    mid = (j+i)/2;
    SwapDouble(&a[i],&a[mid]);
	SwapInt(&index[i], &index[mid]);
    ptr = i;
    sptr = i+1;
    for(x = i+1;x<=j;x++)
    {
        if(a[x] < a[ptr])
        {
            SwapDouble(&a[x],&a[sptr]);
			SwapInt(&index[x],&index[sptr]);
            SwapDouble(&a[ptr],&a[sptr]);
			SwapInt(&index[ptr],&index[sptr]);
			ptr++;
			sptr++;
        }
    }
    return ptr;
}

void QuickSort(double obj[], int index[], int p,int q)
{
    if(p<q)
    {
        int par = Partition(obj,index,p,q);
        QuickSort(obj,index,p,par-1);
        QuickSort(obj,index,par+1,q);
    }
}

int KNNPredict(double** trainFeatures, int* trainIndex, int count, double* testFeature, int length, int N)
{
	//cout << "test" << endl;
	const int MAX_LABEL = 500;
	int i;
	int labels[MAX_LABEL];
	double* dists;
	int* indexes;
	int maxIdx;
	int maxCount;
	//cout << "test0" << endl;
	dists = (double*)malloc(sizeof(double) * count);
	indexes = (int*)malloc(sizeof(int) * count);
	//cout << "test1" << endl; 
	for(i=0; i<count; i++)
	{
		dists[i] = LBPDistance(trainFeatures[i], testFeature, length);
		indexes[i] = i;
	}
	//cout << "test2" << endl; 
	memset(labels, 0, sizeof(int) * MAX_LABEL);

	QuickSort(dists, indexes, 0, count - 1);
	//cout << "test3" << endl; 
	for(i=0; i<N; i++)
	{
		labels[trainIndex[indexes[i]]] ++;
	}
	//cout << "test4" << endl; 
	maxCount = 0;
	for(i=0; i<MAX_LABEL; i++)
	{
		if(labels[i] > maxCount)
		{
			maxIdx = i;
			maxCount = labels[i];
		}
	}
	//cout << "test5" << endl; 
	free(dists);
	free(indexes);
	return maxIdx;
}

Mat LBPFeatureMatforKMeans(vector<vector<int>> lbpfeatures)
{
	int count = 0;
	for(int i = 0; i < lbpfeatures.size(); i++)
	{
		count += lbpfeatures.at(i).size();
	}
	//cout << count << endl;
	Mat OneDimLBPMat(count, 1, CV_8UC1);
	
	int counter = 0;
	int startPoint = 0;
	int endPoint = 0;
	for(int i = 0; i < lbpfeatures.size(); i++)
	{
		endPoint = lbpfeatures.at(i).size();
		for(int j = 0; j < lbpfeatures.at(i).size(); j++)
		{	
			OneDimLBPMat.at<uchar>(counter, 0) = lbpfeatures.at(i).at(j);			
			counter++;
		}
		
		startPoint = endPoint + 1;
	}
	return OneDimLBPMat;
}


Mat MakeKMeansCentersInMat(vector<Mat> vecMatCenters)
{
	int counts = 0;
	for(int i = 0; i < vecMatCenters.size(); i++)
	{
		for(int j = 0; j < vecMatCenters.at(i).rows; j++)
		{
			counts++;
		}
	}

	Mat CenterMat(counts, 1, CV_32FC1);

	int counter = 0;
	for(int i = 0; i < vecMatCenters.size(); i++)
	{
		for(int j = 0; j < vecMatCenters.at(i).rows; j++)
		{
			//cout << "test counter: " << counter << endl;
			CenterMat.at<float>(counter, 0) = vecMatCenters.at(i).at<float>(j, 0);
			counter++;
		}
	}
	
	return CenterMat;
}


vector<Mat> GetClusterLabelMat(vector<vector<vector<int>>> WholeLBPCollection, Mat centers)
{
	vector<Mat> result;
	float diff = 255.0;
	float label = 0;
	//int row = 0;
	
	for(int fcounter = 0; fcounter < WholeLBPCollection.size(); fcounter++)
	{
		Mat temp(WholeLBPCollection.at(fcounter).size(), WholeLBPCollection.at(fcounter).at(0).size(), CV_32FC1);
		
		for(int row = 0; row < WholeLBPCollection.at(fcounter).size(); row++)
		{

			for(int col = 0; col < WholeLBPCollection.at(fcounter).at(row).size(); col++)
			{
				diff = 255.0;
				for(int ctrCount = 0; ctrCount < centers.rows; ctrCount++)
				{ 
					if(diff > fabs(centers.at<float>(ctrCount,0) - WholeLBPCollection.at(fcounter).at(row).at(col)))
					{
						diff = fabs(centers.at<float>(ctrCount,0) - WholeLBPCollection.at(fcounter).at(row).at(col));
						label = centers.at<float>(ctrCount, 0);
					}

				}
				temp.at<float>(row, col) = label;
			}
		}
		//cout << "fcounter1: " << fcounter << endl;
		result.push_back(temp);
		temp.release();
	}
	return result;
}


//Make blocks for one image (based on cluster label collection)
//blockRowNum is the block number per Row
//blockColNum is the block number per Col
vector<Mat> makeVectorMatBlockFromClusterLabel(Mat ClusterLabel, int blockRowNum, int blockColNum)
{
	int row = ClusterLabel.rows;
	int col = ClusterLabel.cols;

	int rowLen = row/blockRowNum;
	int colLen = col/blockColNum;

	vector<Mat> result;

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			Mat block(rowLen, colLen, CV_32FC1);
			for(int brow = 0; brow < rowLen; brow++)
			{
				for(int bcol = 0; bcol < colLen; bcol++)
				{
					block.at<float>(brow, bcol) = ClusterLabel.at<float>(i*rowLen + brow, j*colLen + bcol);
				}
			}
			result.push_back(block);
		}
	}
	return result;
}






//Make blocks for one image (based on LBP code collection)
//blockRowNum is the block number per Row
//blockColNum is the block number per Col
vector<Mat> makeVectorMatBlockFromLBPCollection(vector<vector<int>> LBPCollection, int blockRowNum, int blockColNum)
{
	int row = LBPCollection.size();
	int col = LBPCollection.at(0).size();

	int rowLen = row/blockRowNum;
	int colLen = col/blockColNum;

	vector<Mat> result;

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			Mat block(rowLen, colLen, CV_32FC1);

			for(int brow = 0; brow < rowLen; brow++)
			{
				for(int bcol = 0; bcol < colLen; bcol++)
				{
					block.at<float>(brow, bcol) = (float)LBPCollection.at(i*rowLen + brow).at(j*colLen + bcol);
				}
			}
			result.push_back(block);
		}
	}
	return result;
}


//block is the cluster label Mat.
double getMajorityDensity(Mat block, Mat centers, int& majority)
{
	int size = block.rows * block.cols;

	int count = 0;
	int label = -1;
	//cout << "center.rows: " << centers.rows << endl;
	vector<int> temp(centers.rows);
	temp.assign(centers.rows,0);

	for(int i = 0; i < block.rows; i++)
	{
		for(int j = 0; j < block.cols; j++)
		{
			for(int k = 0; k < centers.rows; k++)
			{
				if((int)block.at<float>(i,j) == (int)centers.at<float>(k,0))
				{
					temp.at(k)++;
				}
			}
		}
	}

	//find the majority cluster label
	int templabel = temp.at(0);
	int idx = 0;
	//cout << temp.at(0) << endl;
	for(int i = 0; i < temp.size(); i++)
	{
		//cout << temp.at(i) << endl;
		if(templabel < temp.at(i))
		{
			templabel = temp.at(i);
			idx = i;
		}
	}
	//cout << "idx: " << idx << endl;
	//system("PAUSE");
	majority = idx;
	double result = (double)templabel/(double)size;
	
	return result;
}

Mat MakeBlockLabelforImage(vector<Mat> block, int blockRowNum, int blockColNum, Mat centers)
{
	int label;
	Mat BlockLabelImage(blockRowNum, blockColNum, CV_32FC1);

	for(int i = 0; i < blockRowNum; i++)
	{
		for(int j = 0; j < blockColNum; j++)
		{
			getMajorityDensity(block.at(i * blockRowNum + j), centers, label);
			BlockLabelImage.at<float>(i, j) = centers.at<float>(label, 0);
			//cout << "label: " << label << " value: "<< centers.at<float>(label, 0) << endl;
		}
	}

	return BlockLabelImage;
}


Mat MakeMatforSVMInput(vector<Mat> blockLabel)
{
	int cols = blockLabel.at(0).rows * blockLabel.at(0).cols;

	Mat SVMInput(blockLabel.size(), cols, CV_32FC1);

	for(int i = 0; i < blockLabel.size(); i++)
	{
		//SVMInput.at<float>(i)
		Mat rowMat(1, cols, CV_32FC1);

		for(int row = 0; row < blockLabel.at(i).rows; row++)
		{
			for(int col = 0; col < blockLabel.at(i).cols; col++)
			{
				rowMat.at<float>(0, row * blockLabel.at(i).rows + col) = blockLabel.at(i).at<float>(row, col);
			}
		}
		rowMat.copyTo(SVMInput.rowRange(i, i+1));
	}

	return SVMInput;
}

Mat MakeMatforLabelFromVector(vector<int> vlabel)
{
	int size = vlabel.size();

	Mat labelmat(size, 1, CV_32FC1);
	for(int i = 0; i < size; i++)
	{
		labelmat.at<float>(i,0) = vlabel.at(i);
	}
	return labelmat;
}

int main(int argc, char* argv[])
{
	int ImgRowBlock = 20;
	int ImgColBlock = 20;

	const int LBP_N_COL = 10;
	const int LBP_N_ROW = 10;
	int ptr;
	int i, j;

	Mat* lbpMap;

	Mat grayImg;
	Mat grayImg32f;
	Mat iiGrayImg32f;

	LBPModel lbpModel;

	ClassInfo* trainInfo;
	ClassInfo* testInfo;

	double** trainFeatures;
	double** testFeatures;

	int* trainIndex;
	int* testIndex;
	int error = 0;

	argv[1] = "C:/Course/C plusplus test/test1/samples";
	//argv[2] = "C:/Course/C plusplus test/test1/testsamples";
	argv[2] = "C:/Course/C plusplus test/test1/samples";
	//argv[2] = "C:/Course/C plusplus test/test1/testset";
	
	trainInfo = TraversClassFolder(argv[1]);
	testInfo = TraversClassFolder(argv[2]);

	trainFeatures = (double**)malloc(sizeof(double*) * trainInfo->fileCount);
	testFeatures = (double**)malloc(sizeof(double*) * testInfo->fileCount);
	trainIndex = (int*)malloc(sizeof(int) * trainInfo->fileCount);
	testIndex = (int*)malloc(sizeof(int) * testInfo ->fileCount);


	//Initialize LBP
	//lbpModel.type = lbpModel.DCV_LBPTYPE_TERNARY;
	lbpModel.type = lbpModel.DCV_LBPTYPE_BINARY;
	lbpModel.scaleRatio = 0.04;
	lbpModel.ternaryRange = 5;
	lbpModel.nNeighbor = 8;
	lbpModel.radius = 1;
	lbpModel.uniformed = 0;
	InitializeLBPModel(&lbpModel);
	



	///For cluster (K-Means)
	Mat labels;
	Mat centers;
	int K = 10;
	///

	vector<int> ClassSeparateCounts;
	vector<int> ClassTestSeparateCounts;


	ptr = 0;
	vector<vector<int>> LBPTrainCollection;
	vector<vector<vector<int>>> WholeLBPTrainCollection;
	vector<vector<vector<int>>> WholeLBPTestCollection;
	vector<vector<int>> LBPTestCollection;
	vector<int> TempLBPCollection;
	vector<Mat> vecMatCenters;
	int counts = 0;

	/////set ExtractFeatureTime for getting beginning time of extracting features.
	clock_t ExtractFeatureTime;
	ExtractFeatureTime = clock();
	cout << "Extracting Training Feature" << endl;


	for(i = 0; i < trainInfo->classCount; i++)
	{
		
		for(j = 0; j < trainInfo->fileCounts[i]; j++)
		{
			cout << "Class: " << i << "  Sample: " << j << endl;
			//grayImg = imread(trainInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_GRAYSCALE);
			Mat Img = imread(trainInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_COLOR);
			Mat dst;
			resize(Img, dst, Size(100, 100), 0.0, 0.0, INTER_CUBIC);
			//imshow("test",dst);
			//waitKey(0);
			Mat CannyEdgeImg = GetCannyEdgeMat(dst);

			cout <<  trainInfo->fileName[i][j].c_str() << endl;
			
			string cannyfile = "Canny\\";
			char cannytempi[10];
			itoa(i, cannytempi, 10);
			char cannytempj[10];
			itoa(j, cannytempj, 10);

			string cannyEdgeFileName = string(trainInfo->fileName[i][j].c_str());
			size_t found = cannyEdgeFileName.find_last_of("\\");
			//cout << found << endl;
			string OutputCannyFileName = cannyEdgeFileName.substr(found+1, cannyEdgeFileName.length());
			//cout << OutputCannyFileName << endl;
			//imwrite(cannyfile + OutputCannyFileName, CannyEdgeImg);
			//imwrite(cannyfile + string(trainInfo->fileName[i][j].c_str()), CannyEdgeImg);


			cvtColor(CannyEdgeImg, grayImg, CV_LOAD_IMAGE_GRAYSCALE);

			grayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);
			iiGrayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);

			grayImg.convertTo(grayImg32f, IPL_DEPTH_32F);
			
			//IlluminationInvairanceProcedure(grayImg, iiGrayImg32f);
			/////////
			Mat centers;
			/////////
			
			lbpMap = GetLBPMap(grayImg32f, &lbpModel, LBPTrainCollection);
			/*imshow("sss",*lbpMap);
			waitKey(0);*/
			/////////K-Means Part for per Image/////////
			//the kmeansMat is used as input for KMeans clustering.
			Mat kmeansMat = LBPFeatureMatforKMeans(LBPTrainCollection);
			kmeansMat.convertTo(kmeansMat, CV_32FC1);
			kmeansMat.copyTo(labels);
			labels.setTo(Scalar(0),noArray());
			kmeans(kmeansMat, K, labels, TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 10, 0.1), 3, cv::KMEANS_PP_CENTERS, centers);
			
			vecMatCenters.push_back(centers);

			
			////////K-Means Part END////////////////////

			trainFeatures[ptr] = ExtractLBPFeature(lbpMap, LBP_N_ROW, LBP_N_COL, &lbpModel);
			trainIndex[ptr] = i;
			ptr++;
			grayImg.release();
			grayImg32f.release();
			iiGrayImg32f.release();
			(*lbpMap).release();
			
			Img.release();
			CannyEdgeImg.release();
			//dst.release();

			WholeLBPTrainCollection.push_back(LBPTrainCollection);
			LBPTrainCollection.clear();

			counts = i;
			ClassSeparateCounts.push_back(counts);////////
		}	
	}

	//system("Pause");
	Mat TrainImagesCenters = MakeKMeansCentersInMat(vecMatCenters);
	Mat TrainImagesLabels;
	Mat TrainWholeCenters;

	TrainImagesCenters.copyTo(TrainImagesLabels);
	TrainImagesLabels.setTo(Scalar(0),noArray());
	kmeans(TrainImagesCenters, K, TrainImagesLabels, TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 10, 0.1), 3, cv::KMEANS_PP_CENTERS, TrainWholeCenters);
	
	vector<Mat> labelCluster = GetClusterLabelMat(WholeLBPTrainCollection, TrainWholeCenters);
	vector<vector<Mat>> blockWholeImages;
	vector<Mat> blockLabelMat;
	
	for(int i = 0; i < labelCluster.size(); i++)
	{
		blockWholeImages.push_back(makeVectorMatBlockFromClusterLabel(labelCluster.at(i), ImgRowBlock, ImgColBlock));
		blockLabelMat.push_back(MakeBlockLabelforImage(makeVectorMatBlockFromClusterLabel(labelCluster.at(i), ImgRowBlock, ImgColBlock), ImgRowBlock, ImgColBlock, TrainWholeCenters));
	}

	//end of extracting features, and show the duration
	clock_t ExtractFeatureTimeEnd = clock();
	double ExtractFeatureDuration = (ExtractFeatureTimeEnd - ExtractFeatureTime)/(double)CLOCKS_PER_SEC;
	cout << "ExtractFeatureDuration: " << ExtractFeatureDuration << endl;
	
	//system("PAUSE");
	Mat svmTrainData = MakeMatforSVMInput(blockLabelMat);
	Mat svmTrainDataLabel = MakeMatforLabelFromVector(ClassSeparateCounts);


	//////////////////////////////////////////////////////////////////////////////
	////////////////////////////SVMs//////////////////////////////////////////////
	
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.gamma = 0.01;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	CvParamGrid CvParamGrid_C(0, 1, 0);
	CvParamGrid CvParamGrid_Gamma;

    // Train the SVM
    CvSVM SVM;
	
	clock_t TrainingTime = clock();
	//SVM.train(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params);
	/*SVM.train_auto(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
		, CvSVM::get_default_grid(CvSVM::GAMMA), CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
		, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);*/
	SVM.train_auto(svmTrainData, svmTrainDataLabel, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
		, CvParamGrid_C, CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
		, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);
	clock_t TrainingTimeEnd = clock();
	double TrainingDuration = (TrainingTimeEnd - TrainingTime)/(double)CLOCKS_PER_SEC;
	cout << "TrainingDuration: " << TrainingDuration << endl;
	//system("PAUSE");


	///////////////////////////////////////////////////////////////////////////////
	////////////////////////////Random Forest//////////////////////////////////////
	/*
	CvRTrees rforest;
	CvRTParams rfParams;
	rfParams.max_depth = 5;
	rfParams.min_sample_count = 1;
	rfParams.max_categories = 2;
	rfParams.calc_var_importance = true;
	rfParams.nactive_vars = 0;
	rfParams.priors = 0;
	rfParams.regression_accuracy = 0.01;
	rfParams.truncate_pruned_tree = true;
	rfParams.term_crit = cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 50, 0.1);

	Mat rfLabel(svmTrainData.rows, 1, CV_32FC1);

	rforest.train(svmTrainData, CV_ROW_SAMPLE, svmTrainDataLabel, Mat(), Mat(), Mat(), Mat(), rfParams);

	int rfcorrect = 0;
	int rferror = 0;
	*/

	/*for(int rfTrainSampleRow = 0; rfTrainSampleRow < svmTrainData.rows; rfTrainSampleRow++)
	{
		double rfPredict = rforest.predict(svmTrainData.row(i), Mat());

		if((int)rfPredict == (int)svmTrainDataLabel.at<float>(i,0))
		{
			rfcorrect++;
		}
		else
		{
			rferror++;
		}


	}
	double rfaccuracy = (double)rfcorrect/(double)svmTrainDataLabel.rows;

	cout << "rfaccuracy: " << rfaccuracy << endl;*/


	//TrainParameters tttparams;
	//TestCompareSVMResponse(svmTrainDataLabel, rfLabel, tttparams);
	
	///////////////////////////////////////////////////////////////////////////////

	cout << endl;

	cout << "Extracting Test Features" << endl;
	ptr = 0;
	int testCount = 0;
	clock_t ExtractTestFeatureTime = clock();

	for(i = 0; i < testInfo->classCount; i++)
	{
		testCount = i;

		for(j = 0; j < testInfo->fileCounts[i]; j++)
		{ 
			cout << "Test Folder: " << i << " Input: " << j << endl;
			//grayImg = imread(testInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_GRAYSCALE);
			Mat Img = imread(testInfo->fileName[i][j].c_str(), CV_LOAD_IMAGE_COLOR);
			Mat dst;
			resize(Img, dst, Size(100, 100), 0.0, 0.0, INTER_CUBIC);

			Mat CannyEdgeImg = GetCannyEdgeMat(dst);
			cvtColor(CannyEdgeImg, grayImg, CV_LOAD_IMAGE_GRAYSCALE);
			//cout << "Input again: " << j << endl;
			grayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);
			iiGrayImg32f = Mat(grayImg.rows, grayImg.cols, CV_32FC1);
			
			grayImg.convertTo(grayImg32f, IPL_DEPTH_32F);
			
			//IlluminationInvairanceProcedure(grayImg, iiGrayImg32f);
			lbpMap = GetLBPMap(grayImg, &lbpModel, LBPTestCollection);
			




			testFeatures[ptr] = ExtractLBPFeature(lbpMap, LBP_N_ROW, LBP_N_COL, &lbpModel);
			testIndex[ptr] = i;
			
			ptr++;
			WholeLBPTestCollection.push_back(LBPTestCollection);
			LBPTestCollection.clear();
			ClassTestSeparateCounts.push_back(testCount);

			
			
			grayImg.release();
			grayImg32f.release();
			iiGrayImg32f.release();
			(*lbpMap).release();
			Img.release();
			CannyEdgeImg.release();
			//dst.release();
		}
		
	}


	vector<Mat> labelTestCluster = GetClusterLabelMat(WholeLBPTestCollection, TrainWholeCenters);
	vector<vector<Mat>> blockWholeTestImages;
	vector<Mat> TestblockLabelMat;
	
	for(int i = 0; i < labelTestCluster.size(); i++)
	{
		blockWholeTestImages.push_back(makeVectorMatBlockFromClusterLabel(labelTestCluster.at(i), ImgRowBlock, ImgColBlock));
		TestblockLabelMat.push_back(MakeBlockLabelforImage(makeVectorMatBlockFromClusterLabel(labelTestCluster.at(i), ImgRowBlock, ImgColBlock), ImgRowBlock, ImgColBlock, TrainWholeCenters));
		//system("PAUSE");
	}

	clock_t ExtractTestFeatureTimeEnd = clock();
	double ExtractTestFeatureDuration = (ExtractTestFeatureTimeEnd - ExtractTestFeatureTime)/(double)CLOCKS_PER_SEC;
	cout << "ExtractTestFeatureDuration: " << ExtractTestFeatureDuration << endl;
	system("PAUSE");
	
	Mat svmTestData = MakeMatforSVMInput(TestblockLabelMat);
	Mat svmTestDataLabel = MakeMatforLabelFromVector(ClassTestSeparateCounts);
	///////////////////////////////////////////////////////////////////////////////
	//////////////////////SVM PREDICTION///////////////////////////////////////////
	Mat outputlabel;

	clock_t TestTime = clock();
	SVM.predict(svmTestData, outputlabel);
	clock_t TestTimeEnd = clock();
	double TestDuration = (TestTimeEnd - TestTime)/(double)CLOCKS_PER_SEC;
	cout << "TestDuration: " << TestDuration << endl;
	//system("PAUSE");

	TrainParameters param;
	param.CannyLowerBound = LOWTHRESHOLD;
	param.CannyUpperBound = UPPERTHRESHOLD;
	param.ColBlockNum = LBP_N_COL;
	param.RowBlockNum = LBP_N_ROW;
	param.innerK = 8;
	param.outerK = 8;
	param.Resized = 0;
	param.params = params;

	TestCompareSVMResponse(svmTestDataLabel, outputlabel, param, testInfo);
	cout << endl;
	
	CvSVMParams getparam = SVM.get_params();
	cout << "gamma: " << getparam.gamma << endl;
	cout << CvParamGrid_C.step << endl;
	//cout << "svmTestDataLabel: " << svmTestDataLabel.rows << endl;
	////////////////////////////////////////////////////////////////////////////////
	
	/*
	for(int i = 0; i < svmTestData.rows; i++)
	{
		double result = rforest.predict(svmTestData.row(i), Mat());

		if((int)result == (int)svmTestDataLabel.at<float>(i,0))
		{
			rfcorrect++;
			cout << "Predict: " << (int)result << " Actual Result: " << (int)svmTestDataLabel.at<float>(i,0) << " Right!" << endl;
		}
		else
		{
			rferror++;
			cout << "Predict: " << (int)result << " Actual Result: " << (int)svmTestDataLabel.at<float>(i,0) << " Wrong!" << endl;

			


		}
	}
	cout << "Random Forest Accuracy: " << (double)rfcorrect/(double)svmTestData.rows << endl;
	*/


	/************************************************************************************************
	cout << "Predict" << endl;
	int tempIdx = 0;
	for(i = 0; i < testInfo->classCount; i++)
	{
		int predictResult;	
		for(j = 0; j < testInfo->fileCounts.at(i); j++)
		{
			predictResult = KNNPredict(trainFeatures, trainIndex, trainInfo->fileCount, testFeatures[tempIdx + j], LBP_N_ROW * LBP_N_COL * lbpModel.codeLength, 3);
			if(predictResult == testIndex[tempIdx + j])
			{
				cout << "FileName: " << testInfo->fileName.at(i).at(j) << endl;
				cout << "NO." << tempIdx + j << " " << testIndex[tempIdx + j] << " -> " << predictResult << " Right" << endl;
			}
			else
			{
				cout << "FileName: " << testInfo->fileName.at(i).at(j) << endl;
				cout << "NO." << tempIdx + j << " " << testIndex[tempIdx + j] << " -> " << predictResult << " Wrong" << endl;
				error++;
			}
			
		}
		tempIdx = j;
	}
	cout << "ERROR: " << error << "/" << testInfo->fileCount << ": " << 1 - (double)error/testInfo->fileCount << endl; 
	*****************************************************************************************/

/*
	cout << endl << endl << endl;
	error = 0;


	for(i = 0; i < testInfo->fileCount; i++)
	{
		int predictResult;		

		predictResult = KNNPredict(trainFeatures, trainIndex, trainInfo->fileCount, testFeatures[i], LBP_N_ROW * LBP_N_COL * lbpModel.codeLength, 10);
		if(predictResult == testIndex[i])
		{
			cout << "NO." << i << " " << testIndex[i] << " -> " << predictResult << " Right" << endl;
		}
		else
		{
			cout << "NO." << i << " " << testIndex[i] << " -> " << predictResult << " Wrong" << endl;
			error++;
		}
	}
	cout << "ERROR: " << error << "/" << testInfo->fileCount << ": " << 1 - (double)error/testInfo->fileCount << endl; 
*/

	//////////////////////////////////////////////
	/////////show cluster centers/////////////////
	/*cout << "cluster centers:" << endl;
	for(int clusterRow = 0; clusterRow < TrainWholeCenters.rows; clusterRow++)
	{
		cout << TrainWholeCenters.at<float>(clusterRow, 0) << endl;
	}*/
	//////////////////////////////////////////////

	return 0;
}
