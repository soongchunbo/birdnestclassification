#include "StdAfx.h"
#include "CannyEdgeDetection.h"

Mat GetCannyEdgeMat(Mat src)
{
	Mat canny_img_mid(src.rows, src.cols, src.type());
	Mat canny_img_result(src.rows, src.cols, src.type());
	bitwise_not(src, canny_img_mid);
	
	Canny(src, canny_img_mid, LOWTHRESHOLD, UPPERTHRESHOLD, 3);
	
	cvZero(&(IplImage)canny_img_result);
	cvCopy(&(IplImage)src, &(IplImage)canny_img_result, &(IplImage)canny_img_mid);
	
	return canny_img_result;
}