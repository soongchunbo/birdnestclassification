#include "StdAfx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string>
#include "IOOperation.h"

using namespace std;
using namespace cv;


struct TrainParameters
{
	bool Resized;
	double CannyLowerBound;
	double CannyUpperBound;
	int innerK;
	int outerK;
	int RowBlockNum;
	int ColBlockNum;
	CvSVMParams params;
	
	int ErrorNum;
	double Accuracy;
};



void TestCheckCenters(Mat centers);

void TestBlockLabelMat(Mat blockLabelMat);

void TestOneDimVector(vector<int> v);

void TestCompareSVMResponse(Mat actualLabel, Mat response, TrainParameters& tparam, ClassInfo* testInfo);

void TestWriteResultToFile(TrainParameters param);