#include "StdAfx.h"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <math.h>


using namespace std;
using namespace cv;

#define LOWTHRESHOLD 10
#define UPPERTHRESHOLD 19

Mat GetCannyEdgeMat(Mat src);