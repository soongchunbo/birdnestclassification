#include "NegativeSample.h"

NegativeSample::NegativeSample(int nWidth, int nHeight, Point2i p1, Point2i p2, Point2i nCenter, Mat nImg, string nSource)
{
	width = nWidth;
	height = nHeight;
	source_leftupper = p1;
	source_rightbottom = p2;

	center = nCenter;

	negativeImg = nImg;
	source = nSource;

	//set initial ratio//
	ratio = 0.0;
	nRatio = 0.0;
	nSelfRatio = 0.0;
}

void NegativeSample::ComparisonRatio(Point2i pLU, Point2i pRB, Point2i pCenter)
{
	int area = abs(pLU.x - pRB.x) * abs(pLU.y - pRB.y);
	int selfArea = width * height;
	int cover_area = 0;

	int LeftBoundx = 1;
	int RightBoundx = 1;
	int LeftBoundy = 1;
	int RightBoundy = 1;

	if(pLU.x > pRB.x)
	{
		int temp = pLU.x;
		pLU.x = pRB.x;
		pRB.x = temp;
	}
	if(pLU.y > pRB.y)
	{
		int temp = pLU.y;
		pLU.y = pRB.y;
		pRB.y = temp;
	}

	if(pLU.x > source_leftupper.x)
	{
		LeftBoundx = pLU.x;
	}
	else
	{
		LeftBoundx = source_leftupper.x;
	}

	if(pRB.x > source_rightbottom.x)
	{
		RightBoundx = source_rightbottom.x;
	}
	else
	{
		RightBoundx = pRB.x;
	}


	if(pLU.y > source_leftupper.y)
	{
		LeftBoundy = pLU.y;
	}
	else
	{
		LeftBoundy = source_leftupper.y;
	}

	if(pRB.y > source_rightbottom.y)
	{
		RightBoundy = source_rightbottom.y;
	}
	else
	{
		RightBoundy = pRB.y;
	}


	if(((RightBoundy - LeftBoundy) > 0) && ((RightBoundx - LeftBoundx) > 0))
	{
		cover_area = (RightBoundx - LeftBoundx) * (RightBoundy - LeftBoundy);
	}
	else
	{
		cover_area = 0;
	}

	ratio = (double)cover_area/(area * 1.0);
	pSelfRatio = (double)cover_area/(selfArea * 1.0);
	//cout << "cover_area: " << cover_area << endl;
	//cout << "area: " << area << endl;
	//cout << "left: " << source_leftupper.x << " " << source_leftupper.y << endl;
	//cout << "ratio: " << ratio << endl;

}


void NegativeSample::ComparisonNRatio(Point2i nLU, Point2i nRB, Point2i nCenter)
{
	int area = abs(nLU.x - nRB.x) * abs(nLU.y - nRB.y);
	int selfArea = width * height;

	int cover_area = 0;

	int LeftBoundx = 1;
	int RightBoundx = 1;
	int LeftBoundy = 1;
	int RightBoundy = 1;

	if(nLU.x > nRB.x)
	{
		int temp = nLU.x;
		nLU.x = nRB.x;
		nRB.x = temp;
	}
	if(nLU.y > nRB.y)
	{
		int temp = nLU.y;
		nLU.y = nRB.y;
		nRB.y = temp;
	}

	if(nLU.x > source_leftupper.x)
	{
		LeftBoundx = nLU.x;
	}
	else
	{
		LeftBoundx = source_leftupper.x;
	}

	if(nRB.x > source_rightbottom.x)
	{
		RightBoundx = source_rightbottom.x;
	}
	else
	{
		RightBoundx = nRB.x;
	}


	if(nLU.y > source_leftupper.y)
	{
		LeftBoundy = nLU.y;
	}
	else
	{
		LeftBoundy = source_leftupper.y;
	}

	if(nRB.y > source_rightbottom.y)
	{
		RightBoundy = source_rightbottom.y;
	}
	else
	{
		RightBoundy = nRB.y;
	}


	if(((RightBoundy - LeftBoundy) > 0) && ((RightBoundx - LeftBoundx) > 0))
	{
		cover_area = (RightBoundx - LeftBoundx) * (RightBoundy - LeftBoundy);
	}
	else
	{
		cover_area = 0;
	}

	nRatio = (double)cover_area/(area * 1.0);
	nSelfRatio = (double)cover_area/(selfArea * 1.0);
	//cout << endl << endl;
	//cout << "selfArea " << selfArea << endl << endl << endl;
	//cout << "cover_area: " << cover_area << endl;
	//cout << "area: " << area << endl;
	//cout << "left: " << source_leftupper.x << " " << source_leftupper.y << endl;
	//cout << "ratio: " << ratio << endl;

}




void NegativeSample::CollectRatio(double fratio)
{
	vRatio.push_back(fratio);
}

void NegativeSample::CollectSPRatio(double ssratio)
{
	vpSelfRatio.push_back(ssratio);
}


void NegativeSample::WriteNSToFile(string dir, string ffname)
{
	string fname = dir + ffname + ".jpg";
	cout << fname << endl;
	if(imwrite(fname, negativeImg))
		cout << "done." << endl;
	else
		cout << "fail." << endl;
}

void NegativeSample::WriteScriptToFile(FILE* fp, int num, string nsfilename)
{
	//cout << "aaa" << endl;
	const char* cSource = source.c_str();
	char cIntWidth[10];
	itoa(width, cIntWidth, 10);
	char cIntHeight[10];
	itoa(height, cIntHeight, 10);
	
	char cIntCenterX[10];
	itoa(center.x, cIntCenterX, 10);

	char cIntCenterY[10];
	itoa(center.y, cIntCenterY, 10);

	char cNum[10];
	itoa(num, cNum, 10);

	
	for(int i = 0; i < vRatio.size(); i++)
	{
		char cDoubleRatio[10];
		double VR = vRatio.at(i);
		sprintf(cDoubleRatio, "%lf", VR);

		char cti[10];
		itoa(i, cti, 10);

		string result = string(cNum) + "   " + nsfilename + "  " + string(cSource) + ", " + string(cIntWidth)
					+ ", " + string(cIntHeight)	+ ", " + string(cIntCenterX) + ", " 
					+ string(cIntCenterY) + ", " + "Bounding Box: " + string(cti) + "-" + string(cDoubleRatio) + "\n";

		fputs(result.c_str(), fp);

	}

}