#include <stdio.h>
#include <iostream>
#include <opencv.hpp>
//#include <core\core.hpp>
#include <opencv2\core\core.hpp>

using namespace std;
using namespace cv;

class PIMGBBOX
{
public:

	string source;
	string source_name;

	Point2i left_upper;
	Point2i right_bottom;

	Point2i center;
	Mat bounding_box;

	int bounding_box_height;
	int bounding_box_width;

	double ratio;

	//PIMGBBOX();
	PIMGBBOX(Point2i, Point2i, string, Mat, const int, string);
	
	//void get_bbox_height();

	//void get_bbox_width();

	void showcontent();

	void showboundingbox();

	void WriteBboxToFile(string dir, string);

	void RatioBboxWImg(Mat);
};