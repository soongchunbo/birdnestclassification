#include <stdio.h>
#include <iostream>
#include <opencv.hpp>
//#include <core\core.hpp>
#include <opencv2\core\core.hpp>
#include <vector>

using namespace std;
using namespace cv;

class NegativeSample
{
public:
	int width;
	int height;

	string source;

	Point2i source_leftupper;
	Point2i source_rightbottom;
	Point2i center;

	double ratio;
	vector<double> vRatio;

	double pSelfRatio;
	vector<double> vpSelfRatio;

	double nRatio;
	vector<double> vNRatio;

	double nSelfRatio;
	vector<double> vnSelfRatio;

	Mat negativeImg;

	NegativeSample(int nWidth, int nHeight, Point2i p1, Point2i p2, Point2i nCenter, Mat nImg, string nSource);

	void ComparisonRatio(Point2i pLU, Point2i pRB, Point2i pCenter);

	void ComparisonNRatio(Point2i nLU, Point2i nRB, Point2i nCenter);

	void CollectRatio(double ratio);
	void CollectSPRatio(double selfRatio);

	void WriteNSToFile(string dir, string);

	void WriteScriptToFile(FILE* fp, int num, string nsfilename);
};