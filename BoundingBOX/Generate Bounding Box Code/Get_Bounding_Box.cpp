#include "Get_Bounding_Box.h"

//PIMGBBOX::PIMGBBOX()
//{
//	//PIMGBBOX(Point2i(1,1), Point2i(1,1), NULL, 16);
//}

PIMGBBOX::PIMGBBOX(Point2i leftupper, Point2i rightbottom, string name, Mat filesouce, const int type, string flname)
{
	int height = 0;
	int width = 0;

	//source = filesouce;
	source_name = flname;
	left_upper.x = leftupper.x;
	left_upper.y = leftupper.y;

	right_bottom.x = rightbottom.x;
	right_bottom.y = rightbottom.y ;


	height = rightbottom.y - leftupper.y;
	width = rightbottom.x - leftupper.x;

	center.x = (left_upper.x + right_bottom.x)/2;
	center.y = (left_upper.y + right_bottom.y)/2;

	height = abs(height);
	width = abs(width);

	source = name;

	//bounding_box = new Mat(Size(abs(width), abs(height)), type);
	bounding_box = filesouce;
	bounding_box_height = height;
	bounding_box_width = width;
	ratio = 0.0;
}

void PIMGBBOX::showcontent()
{
	cout << source << endl;
	cout << left_upper.x << "," << left_upper.y << endl;
	cout << right_bottom.x << "," << right_bottom.y << endl;

	this->showboundingbox();
}

void PIMGBBOX::showboundingbox()
{
	namedWindow(source, 1);
	imshow(source, bounding_box);
	waitKey(0);
}

void PIMGBBOX::WriteBboxToFile(string dir, string ffname)
{
	
	string fname = dir + ffname + "-" + source_name + ".jpg";
	cout << fname << endl;
	if(imwrite(fname, bounding_box))
		cout << "done." << endl;
	else
		cout << "fail." << endl;

	//cout << center.x << " " << center.y << endl;
}

void PIMGBBOX::RatioBboxWImg(Mat wholeImg)
{
	int area = wholeImg.cols * wholeImg.rows;
	int CoverArea = bounding_box_height * bounding_box_width;

	ratio = (double)CoverArea/area*1.0;
	//cout << "positive ratio: " << ratio << endl;
}