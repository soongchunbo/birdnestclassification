// Generating_Negative_Sample.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <stdio.h>
#include <imgproc\imgproc.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <vector>
#include <math.h>
#include "stdafx.h"
#include "Get_Bounding_Box.h"
#include "NegativeSample.h"
//#include "CandidateBox.h"


using namespace std;
using namespace cv;

//Read File to get Coordinates, 
//lu is the left upper point of bounding box subimage,
//rb is the right bottom point of bounding box subimage.
void Get_Coord(string instr, Point2i& lu, Point2i& rb)
{
	int length = instr.length();
	bool pointflag = true;
	string lutemp_coord_x;
	string lutemp_coord_y;
	string rbtemp_coord_x;
	string rbtemp_coord_y;
	bool xyflag1 = true;
	//bool xyflag2 = true;
	int temp_idx = 0;

	for(int i = 0; instr.c_str()[i] != ' '; i++)
	{
		if(instr.c_str()[i] == '(')
		{	
			xyflag1 = true;
			continue;
		}
		if(instr.c_str()[i] != ',' && xyflag1)
		{
			lutemp_coord_x += instr.c_str()[i];
		}
		if(instr.c_str()[i] == ',')
		{
			xyflag1 = false;
			continue;
		}
		if(instr.c_str()[i] != ')' && (!xyflag1))
		{
			lutemp_coord_y += instr.c_str()[i];
		}
		if(instr.c_str()[i] == ')')
		{
			temp_idx = i + 2;
		}
	}
	lu.x = atoi(lutemp_coord_x.c_str());
	lu.y = atoi(lutemp_coord_y.c_str());

	for(int i = temp_idx; instr.c_str()[i] != ')'; i++)
	{
		if(instr.c_str()[i] == '(')
		{	
			xyflag1 = true;
			continue;
		}
		if(instr.c_str()[i] != ',' && xyflag1)
		{
			rbtemp_coord_x += instr.c_str()[i];
		}
		if(instr.c_str()[i] == ',')
		{
			xyflag1 = false;
			continue;
		}
		if(instr.c_str()[i] != ')' && (!xyflag1))
		{
			rbtemp_coord_y += instr.c_str()[i];
		}

	}
	rb.x = atoi(rbtemp_coord_x.c_str());
	rb.y = atoi(rbtemp_coord_y.c_str());

}

void GetRawFileName(string raw, string& output)
{
	

	for(int i = 0; raw.c_str()[i] != '.'; i++)
	{
		output += raw.c_str()[i];
	}
	//output = string(OriginalFileName);

}


void TintBboxWholeImg(Mat wholeImg, Point2i lu, Point2i rb, string filename)
{
	Mat temp = wholeImg;
	/*Vec3b Pcolor;
	Pcolor.val[0] = 255;
	Pcolor.val[1] = 0;
	Pcolor.val[2] = 0;
	*/
	Scalar spColor;
	spColor.val[0] = 255;
	spColor.val[1] = 0;
	spColor.val[2] = 0;
	spColor.val[3] = 0;

	rectangle(temp, lu, rb, spColor, 5, 8, 0);

	/*for(int i = lu.x; i <= rb.x; i++)
	{
		for(int j = lu.y; j <= rb.y; j++)
		{
			temp.at<Vec3b>(Point(i,j))[0] = Pcolor.val[0];
			temp.at<Vec3b>(Point(i,j))[1] = Pcolor.val[1];
			temp.at<Vec3b>(Point(i,j))[2] = Pcolor.val[2];
		}
	}*/

	imwrite("TintOriginalImgs\\" + filename, temp);
}


//Get single Bounding Box image from the whole Image.
void Get_Bbox_Img(Point2i lu, Point2i rb, Mat wholeImg, Mat& subImg, string filename)
{
	int start_x = lu.x;
	int start_y = lu.y;
	int end_x = rb.x;
	int end_y = rb.y;
	int temp;

	if(start_x > end_x)
	{
		temp = start_x;
		start_x = end_x;
		end_x = temp;
	}

	if(start_y > end_y)
	{
		temp = start_y;
		start_y = end_y;
		end_y = temp;
	}

	Mat tempImg(wholeImg.rows, wholeImg.cols, wholeImg.type());
	wholeImg.copyTo(tempImg);
	
	tempImg.colRange(start_x, end_x).rowRange(start_y, end_y).copyTo(subImg);
	//TintBboxWholeImg(wholeImg, lu, rb, filename);
}

//Read Boundingbox file to get corresponding Positive SubImage, and put it into container bbox_storage. 
void Read_File(FILE* filelist, vector<vector<PIMGBBOX>>& VPImgBbox)
{
	system("dir wholeImgs /b /od > filelist.txt");

	char buffer[1000];
	string SourceName;


	vector<PIMGBBOX> bbox_storage;
	//read file
	while(fgetc(filelist) != EOF)
	{
		SourceName = "";
		string temp_filename;
		Point2i lu;
		Point2i rb;
		int temp_idx_bboxnum = 0;
		int num_bbox = 0;

		PIMGBBOX* boundingbox;
		Mat wholeImg;
		Mat tempwholeImg;

		//back to the start location for per line of the file.
		fseek(filelist, -1, SEEK_CUR);
		fgets(buffer, 1000, filelist);

		//find the first ' ' to get the index.
		for(int i = 0; buffer[i] != ' '; i++)
		{
			temp_filename += buffer[i];
			temp_idx_bboxnum = i + 2;
		}

		string filename = "wholeImgs\\" + temp_filename;
		wholeImg = imread(filename, 1);
		wholeImg.copyTo(tempwholeImg);


		GetRawFileName(temp_filename, SourceName);

		cout << SourceName << endl;

		string temp_num;
		int temp_idx = 0;

		//find the second ' ' to get the index.
		for(int i = temp_idx_bboxnum; buffer[i] != ' '; i++)
		{
			temp_num += buffer[i];
			temp_idx = i + 2;
		}
		num_bbox = atoi(temp_num.c_str());

		//because the num_instances is the # of the bounding box in the Image,
		//read them all and generate all these bounding box subImage.
		for(int num_instances = 0; num_instances < num_bbox; num_instances++)
		{
			string stemp_bbox_coord = "";

			for(int i = temp_idx; buffer[i] != '\n'; i++)
			{
				if (buffer[i] != ';')
				{
					stemp_bbox_coord += buffer[i];
				}
				else
				{
					cout << stemp_bbox_coord << endl;

					//get coordinates
					Get_Coord(stemp_bbox_coord, lu, rb);
					//cout << abs(rb.y-lu.y) << " " << abs(rb.x-lu.x) << endl;
					Mat subImg(abs(rb.y-lu.y), abs(rb.x-lu.x), wholeImg.type());
					Get_Bbox_Img(lu, rb, wholeImg, subImg, temp_filename);

					TintBboxWholeImg(tempwholeImg, lu, rb, temp_filename);

					boundingbox = new PIMGBBOX(lu, rb, filename, subImg, wholeImg.type(), SourceName);
					bbox_storage.push_back(*boundingbox);

					stemp_bbox_coord.clear();
					temp_idx = i + 1;
					subImg.release();
					break;
				}

			}

		}
		VPImgBbox.push_back(bbox_storage);
		bbox_storage.clear();
	}

}


//void GenerateGrid(int& nWidth, int& nHeight, Mat wholeImg, vector<PIMGBBOX> bBoxes, RNG& nRNG, int attempts)
//{
//	
//	//get average height and width of bounding boxes in one image.
//	int sumWidth = 0;
//	int sumHeight = 0;
//	int totalTimes = 0;
//	for(int i = 0; i < bBoxes.size(); i++)
//	{
//		sumWidth += bBoxes.at(i).bounding_box_width;
//		sumHeight += bBoxes.at(i).bounding_box_height;
//		totalTimes = i + 1;
//	}
//
//	int averWidth = floor((sumWidth * 1.0) /(totalTimes * 1.0));
//	int averHeight = floor((sumHeight * 1.0) /(totalTimes * 1.0));
//
//	nWidth = averWidth;
//	nHeight = averHeight;
//	
//
//	int totalRowNumber = wholeImg.rows/nHeight;
//	int totalColNumber = wholeImg.cols/nWidth;
//
//	vector<CandidateBox> Grids;
//
//	//Generate Grids and label each grid with different number
//	for(int hIdx = 0; hIdx < wholeImg.rows; hIdx = hIdx + averHeight)
//	{
//
//		for(int wIdx = 0; wIdx < wholeImg.cols; wIdx = wIdx + averWidth)
//		{
//			CandidateBox cBox((hIdx*totalColNumber + wIdx), wIdx, hIdx);
//			Grids.push_back(cBox);
//			//GridsIdx.push_back(hIdx*averWidth + wIdx);
//		}
//	}
//
//	//random pick a number for selecting a grid
//	//the grid provides the lower bound and the upper bound for coordinates.
//	int gridPickNum = nRNG.uniform(0, Grids.size()-1);
//
//
//	int gridPickRow = Grids.at(gridPickNum).gridNum / totalColNumber;
//	int gridPickCol = Grids.at(gridPickNum).gridNum % totalColNumber;
//
//	//generate random center within the grid
//	Point2i center;
//	//center.x = nRNG.uniform(gridPickCol*nWidth, (gridPickCol+1)*nWidth);
//	//center.y = nRNG.uniform(gridPickRow*nHeight, (gridPickRow+1)*nHeight);
//	center.x = (gridPickCol*nWidth + (gridPickCol+1)*nWidth)/2;
//	center.y = (gridPickRow*nHeight + (gridPickRow+1)*nHeight)/2;
//
//
//	//generate random width and height for the negative sample
//	nWidth = nRNG.uniform(nWidth*0.9, nWidth*1.1);
//	nHeight = nRNG.uniform(nHeight*0.9, nHeight*1.1);
//
//}


//Generate random Negative Subimage Center.
void GenerateRandomCenter(Point2i& center, int& nWidth, int& nHeight, Mat wholeImg, PIMGBBOX bBox, RNG& nRNG, int avgWidth, int avgHeight)
{
	//Random width and height of per Negative SubImage.
	//RNG nRNG;

	//try to generate a Negative SubImage Center from current Image.
	//nWidth = nRNG.uniform((int)(bBox.bounding_box_width * 0.9), (int)(1.1*bBox.bounding_box_width));
	//nHeight = nRNG.uniform((int)(bBox.bounding_box_height * 0.9), (int)(1.1*bBox.bounding_box_height));
	//
	nWidth = nRNG.uniform((int)(avgWidth * 0.9), (int)(1.1*avgWidth));
	nHeight = nRNG.uniform((int)(avgHeight * 0.9), (int)(1.1*avgHeight));
	while(1)
	{
		
		int width = wholeImg.cols;
		int height = wholeImg.rows;

		//random center position.
		center.x = rand() % width;
		center.y = rand() % height;

		///calculate the distance between the center point and the bounding box center	
		if(center.x - (nWidth/2) >= 0			&&
			center.y - (nHeight/2) >= 0			&&
			center.x + (nWidth/2) <= width - 1	&&
			center.y + (nHeight/2) <= height - 1)
		{

			double res = norm(bBox.center - center);

			//if the distance satisfies the constraints, 
			//break the loop, thus we get a possible Negative SubImage.
			//Otherwise, reduce the width and height of the Negative SubImage,
			//untill the constraints can be satisfied. 
			if((int)res >= ((bBox.bounding_box_height/2) + (bBox.bounding_box_width/2))/2)
			{
				
				cout << "width: " << nWidth << endl;
				cout << "height: " << nHeight << endl;
				cout << "pLU: " << bBox.left_upper.x << "," << bBox.left_upper.y << endl;
				cout << "pRB: " << bBox.right_bottom.x << "," << bBox.right_bottom.y << endl;
				cout << "nLU: "<< center.x - (nWidth/2) << "," << center.y - (nHeight/2) << endl;
				cout << "nRB: " << center.x + (nWidth/2) << "," << center.y + (nHeight/2) << endl << endl;
				break;
			}
			else
			{
				cout << "center.x " << center.x << endl;
				cout << "center.y " << center.y << endl;
				continue;
			}
		}
		else
		{
			//here, choose half of both the width and height.
			nWidth = nWidth * 0.9;
			nHeight = nHeight* 0.9;


			if(nWidth < bBox.bounding_box_width * 0.9 || nHeight < bBox.bounding_box_height * 0.9)
			{
				continue;
			}

		}		
	}
}



void TintNegImgToWholeImg(Mat wholeImg, Point2i lu, Point2i rb, string filename)
{
	Mat temp = wholeImg;
	
	Scalar spColor;
	spColor.val[0] = 0;
	spColor.val[1] = 0;
	spColor.val[2] = 255;
	spColor.val[3] = 0;

	rectangle(temp, lu, rb, spColor, 5, 8, 0);

	
	imwrite("TintOriginalImgs\\" + filename, temp);
}



void TintNegativeImgsToWholeImg(vector<vector<NegativeSample>> TotalNegSample)
{
	system("dir TintOriginalImgs /b /od > TintOriginalImgs.txt");
	FILE* fp = fopen("TintOriginalImgs.txt", "r");
	char buffer[1000];
	int Idx_TotalNegSample = 0;

	while(fgetc(fp) != EOF)
	{
		fseek(fp, -1, SEEK_CUR);
		fgets(buffer, 1000, fp);

		string tbuffer;
		for(int i = 0; buffer[i] != '\n'; i++)
		{
			tbuffer += buffer[i];
		}

		cout << string(tbuffer) << endl;
		string filename = "TintOriginalImgs\\" + tbuffer;
		//cout << filename << endl;
		Mat wholeImg = imread(filename, 1);

		int num = TotalNegSample.at(Idx_TotalNegSample).size();
		if(num > 10)
			num = 10;

		for(int i = 0; i < num; i++)
		{
			//cout << "Idx_TotalNegSample " << Idx_TotalNegSample << " i: " << i << endl;
			TintNegImgToWholeImg(wholeImg, TotalNegSample.at(Idx_TotalNegSample).at(i).source_leftupper, TotalNegSample.at(Idx_TotalNegSample).at(i).source_rightbottom, tbuffer);
		
		
		}
		Idx_TotalNegSample++;
	}

	fclose(fp);
}


int main(int argc, char* argv[])
{
	FILE* fp;
	fp = fopen("boundingbox.txt", "r");
	vector<PIMGBBOX> bbox_storage;
	
	int avgBboxWidth = 0;
	int avgBboxHeight = 0;
	
	vector<vector<PIMGBBOX>> PbboxStorage;
	vector<vector<NegativeSample>> TotalNegSample;
	
	Read_File(fp, PbboxStorage);

	//Random width and height of per Negative SubImage.
	RNG nRNG;
	int randNum = rand();


	cout << endl << endl;

	Point2i nCenter;

	for(int i = 0; i < PbboxStorage.size(); i++)
	{
		vector<NegativeSample> NegSample;
		
		char ti[20];
		
		itoa(i,ti,10);
		Mat wImg;

		double sumRatio = 0.0;
		int RatioToTime = 0;
		int nWidth = 0;
		int nHeight = 0;
		int totalArea = 0;
		int wImgArea = 0;

		int totalBboxWidth = 0;
		int totalBboxHeight = 0;

		for(int j = 0; j < PbboxStorage.at(i).size(); j++)
		{
			char tj[20];
			itoa(j, tj, 10);
			
			PbboxStorage.at(i).at(j).WriteBboxToFile("BoundingBox\\", "boundingbox" + string(ti) + "-" + string(tj));

			wImg = imread(PbboxStorage.at(i).at(j).source, 1);
			
			PbboxStorage.at(i).at(j).RatioBboxWImg(wImg);

			totalBboxWidth += PbboxStorage.at(i).at(j).bounding_box_width;
			totalBboxHeight += PbboxStorage.at(i).at(j).bounding_box_height;
			//double pRatioToTime = log(PbboxStorage.at(i).at(j).ratio)/log(2.0);
			totalArea += PbboxStorage.at(i).at(j).bounding_box_height * PbboxStorage.at(i).at(j).bounding_box_width;		
			//sumRatio += pRatioToTime;
		}

		avgBboxWidth = totalBboxWidth/PbboxStorage.at(i).size();
		avgBboxHeight = totalBboxHeight/PbboxStorage.at(i).size();

		wImgArea = wImg.cols * wImg.rows;
		//RatioToTime = abs(2 * (int)ceil(sumRatio));
		RatioToTime = floor(double(wImgArea)/double(totalArea));

		for(int time = 0; time < RatioToTime; time++)
		{
			
			///can use random number as the index of the bounding box.
			int IdxBbox = randNum % PbboxStorage.at(i).size();
			GenerateRandomCenter(nCenter, nWidth, nHeight, wImg, PbboxStorage.at(i).at(IdxBbox), nRNG, avgBboxWidth, avgBboxHeight);
			
			////no less than the limits			
			if(nWidth < (avgBboxWidth * 0.85) || nHeight < (avgBboxHeight * 0.85))
			{
				//cout << "too small, continue." << endl;
				continue;
			}
			

			Mat nImg(nHeight, nWidth, wImg.type());
			wImg.colRange(nCenter.x - (nWidth/2), nCenter.x + (nWidth/2)).rowRange(nCenter.y - (nHeight/2), nCenter.y + (nHeight/2)).copyTo(nImg);

			Point2i nLU, nRB;
			nLU.x = nCenter.x - (nWidth/2);
			nLU.y = nCenter.y - (nHeight/2);
			nRB.x = nCenter.x + (nWidth/2);
			nRB.y = nCenter.y + (nHeight/2);

			NegativeSample NSample(nWidth, nHeight, nLU, nRB, nCenter, nImg, PbboxStorage.at(i).at(0).source);

			for(int j = 0; j < PbboxStorage.at(i).size(); j++)
			{
				NSample.ComparisonRatio(PbboxStorage.at(i).at(j).left_upper, PbboxStorage.at(i).at(j).right_bottom, PbboxStorage.at(i).at(j).center);
				NSample.CollectRatio(NSample.ratio);
				NSample.CollectSPRatio(NSample.pSelfRatio);
			}

			for(int j = 0; j < PbboxStorage.at(i).size(); j++)
			{
				
				if(NSample.vRatio.at(j) <= 0.3 && NSample.vpSelfRatio.at(j) <= 0.3)
				{
					if(j == (PbboxStorage.at(i).size()-1))
					{
						NegSample.push_back(NSample);
					}
				}
				else
				{
					break;
				}

			}
			
		}
		wImg.release();
		
		TotalNegSample.push_back(NegSample);
		
	}

	vector<vector<NegativeSample>> FinalNegSample;
	

	for(int i = 0; i < TotalNegSample.size(); i++)
	{
		vector<NegativeSample> FinalNegSubImgs;
		for(int j = 0; j < TotalNegSample.at(i).size(); j++)
		{
			bool npushFlag = true;
			for(int k = j+1; k < TotalNegSample.at(i).size(); k++)
			{

				TotalNegSample.at(i).at(j).ComparisonNRatio(TotalNegSample.at(i).at(k).source_leftupper, TotalNegSample.at(i).at(k).source_rightbottom, TotalNegSample.at(i).at(k).center);

				if(TotalNegSample.at(i).at(j).nRatio > 0.3 || TotalNegSample.at(i).at(j).nSelfRatio > 0.3)
				{

					npushFlag = false;
				}


			}
			if(npushFlag)
				FinalNegSubImgs.push_back(TotalNegSample.at(i).at(j));
		}
		FinalNegSample.push_back(FinalNegSubImgs);
	}

	//write Negative SubImage to file (generate .JPG file of Negative SubImage)
	//and generate the script of Negative SubImages.	
	FILE* nfp = fopen("NegativeSample.txt", "w+");
	char* titles = "Num			Source	    Width Height Center.x Center.y Ratio\n";
	fputs(titles, nfp);
	
	for(int i = 0; i < FinalNegSample.size(); i++)
	{
		char ti[20];
		itoa(i,ti,10);
		
		int StNum = FinalNegSample.at(i).size();
		if(StNum > 10)
		{
			StNum = 10;
		}

		for(int j = 0; j < StNum; j++)
		{
			char tj[20];
			itoa(j, tj, 10);
			FinalNegSample.at(i).at(j).WriteNSToFile("NegativeSamples\\", "NS" + string(ti) + "-" + string(tj));
			FinalNegSample.at(i).at(j).WriteScriptToFile(nfp, i, string(ti) + "-" + string(tj) + ".jpg");
		}
	}

	TintNegativeImgsToWholeImg(FinalNegSample);
	fclose(fp);
	return 0;

}

