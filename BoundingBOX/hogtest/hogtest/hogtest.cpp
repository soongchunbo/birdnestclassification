﻿// hogtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "fstream"
#include <opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\gpu\gpu.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\objdetect\objdetect.hpp>

#include "IOOperation.h"
#include "test_imgshow.h"


using namespace std;
using namespace cv;
using namespace gpu;


class Mysvm: public CvSVM
{
public:
	int get_alpha_count()
	{
		return this->sv_total;
	}

	int get_sv_dim()
	{
		return this->var_all;
	}

	int get_sv_count()
	{
		return this->decision_func->sv_count;
	}

	double* get_alpha()
	{
		return this->decision_func->alpha;
	}

	float** get_sv()
	{
		return this->sv;
	}

	float get_rho()
	{
		return this->decision_func->rho;
	}
};


//input: table
//output: copy the data of of the table to the Mat type object.
//ATTENTION: NEW => MAT(# of rows, # of cols, type).
Mat _createMat(vector<vector<float>> table)
{
	int table_length = table.at(0).size();
	int table_height = table.size();

	//cout << table_height << endl;

	Mat dataset(table_height, table_length, CV_32FC1);

	if(table.empty())
	{
		cout << "There is no data!" << endl;
		return dataset;
	}

	for(int i = 0; i < table_length; i++)
	{
		for(int j = 0; j < table_height; j++)
		{
			dataset.at<float>(j, i) = (float)table.at(j).at(i);
		}
	}
	return dataset;
}



// HOGDescriptor visual_imagealizer
// adapted for arbitrary size of feature sets and training images
Mat get_hogdescriptor_visual_image(Mat& origImg,
                                   vector<float>& descriptorValues,
                                   Size winSize,
                                   Size cellSize,                                   
                                   int scaleFactor,
                                   double viz_factor)
{   
    Mat visual_image;
    resize(origImg, visual_image, Size(origImg.cols*scaleFactor, origImg.rows*scaleFactor));
 
    int gradientBinSize = 9;
    // dividing 180° into 9 bins, how large (in rad) is one bin?
    float radRangeForOneBin = 3.14/(float)gradientBinSize; 
 
    // prepare data structure: 9 orientation / gradient strenghts for each cell
	int cells_in_x_dir = winSize.width / cellSize.width;
    int cells_in_y_dir = winSize.height / cellSize.height;
    int totalnrofcells = cells_in_x_dir * cells_in_y_dir;
    float*** gradientStrengths = new float**[cells_in_y_dir];
    int** cellUpdateCounter   = new int*[cells_in_y_dir];
    for (int y=0; y<cells_in_y_dir; y++)
    {
        gradientStrengths[y] = new float*[cells_in_x_dir];
        cellUpdateCounter[y] = new int[cells_in_x_dir];
        for (int x=0; x<cells_in_x_dir; x++)
        {
            gradientStrengths[y][x] = new float[gradientBinSize];
            cellUpdateCounter[y][x] = 0;
 
            for (int bin=0; bin<gradientBinSize; bin++)
                gradientStrengths[y][x][bin] = 0.0;
        }
    }
 
    // nr of blocks = nr of cells - 1
    // since there is a new block on each cell (overlapping blocks!) but the last one
    int blocks_in_x_dir = cells_in_x_dir - 1;
    int blocks_in_y_dir = cells_in_y_dir - 1;
 
    // compute gradient strengths per cell
    int descriptorDataIdx = 0;
    int cellx = 0;
    int celly = 0;
 
    for (int blockx=0; blockx<blocks_in_x_dir; blockx++)
    {
        for (int blocky=0; blocky<blocks_in_y_dir; blocky++)            
        {
            // 4 cells per block ...
            for (int cellNr=0; cellNr<4; cellNr++)
            {
                // compute corresponding cell nr
                int cellx = blockx;
                int celly = blocky;
                if (cellNr==1) celly++;
                if (cellNr==2) cellx++;
                if (cellNr==3)
                {
                    cellx++;
                    celly++;
                }
 
                for (int bin=0; bin<gradientBinSize; bin++)
                {
                    float gradientStrength = descriptorValues[ descriptorDataIdx ];
                    descriptorDataIdx++;
 
                    gradientStrengths[celly][cellx][bin] += gradientStrength;
 
                } // for (all bins)
 
 
                // note: overlapping blocks lead to multiple updates of this sum!
                // we therefore keep track how often a cell was updated,
                // to compute average gradient strengths
                cellUpdateCounter[celly][cellx]++;
 
            } // for (all cells)
 
 
        } // for (all block x pos)
    } // for (all block y pos)
 
 
    // compute average gradient strengths
    for (int celly=0; celly<cells_in_y_dir; celly++)
    {
        for (int cellx=0; cellx<cells_in_x_dir; cellx++)
        {
 
            float NrUpdatesForThisCell = (float)cellUpdateCounter[celly][cellx];
 
            // compute average gradient strenghts for each gradient bin direction
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
            }
        }
    }
 
 
    cout << "descriptorDataIdx = " << descriptorDataIdx << endl;
 
    // draw cells
    for (int celly=0; celly<cells_in_y_dir; celly++)
    {
        for (int cellx=0; cellx<cells_in_x_dir; cellx++)
        {
            int drawX = cellx * cellSize.width;
            int drawY = celly * cellSize.height;
 
            int mx = drawX + cellSize.width/2;
            int my = drawY + cellSize.height/2;
 
            rectangle(visual_image,
                      Point(drawX*scaleFactor,drawY*scaleFactor),
                      Point((drawX+cellSize.width)*scaleFactor,
                      (drawY+cellSize.height)*scaleFactor),
                      CV_RGB(100,100,100),
                      1);
 
            // draw in each cell all 9 gradient strengths
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                float currentGradStrength = gradientStrengths[celly][cellx][bin];
 
                // no line to draw?
                if (currentGradStrength==0)
                    continue;
 
                float currRad = bin * radRangeForOneBin + radRangeForOneBin/2;
 
                float dirVecX = cos( currRad );
                float dirVecY = sin( currRad );
                float maxVecLen = cellSize.width/2;
                float scale = viz_factor; // just a visual_imagealization scale,
                                          // to see the lines better
 
                // compute line coordinates
                float x1 = mx - dirVecX * currentGradStrength * maxVecLen * scale;
                float y1 = my - dirVecY * currentGradStrength * maxVecLen * scale;
                float x2 = mx + dirVecX * currentGradStrength * maxVecLen * scale;
                float y2 = my + dirVecY * currentGradStrength * maxVecLen * scale;
 
                // draw gradient visual_imagealization
                line(visual_image,
                     Point(x1*scaleFactor,y1*scaleFactor),
                     Point(x2*scaleFactor,y2*scaleFactor),
                     CV_RGB(0,255,0),
                     1);
 
            } // for (all bins)
 
        } // for (cellx)
    } // for (celly)
 
 
    // don't forget to free memory allocated by helper data structures!
    for (int y=0; y<cells_in_y_dir; y++)
    {
      for (int x=0; x<cells_in_x_dir; x++)
      {
           delete[] gradientStrengths[y][x];            
      }
      delete[] gradientStrengths[y];
      delete[] cellUpdateCounter[y];
    }
    delete[] gradientStrengths;
    delete[] cellUpdateCounter;
 
    return visual_image;
}

//Mat _cropImage(Mat source, Size size, Point point)
//{
//	if(size.height < source.rows && size.width < source.cols)
//	{
//		Rect tempImg(point.x, point.y, size.width, size.height);
//		Mat croppedImg = source(tempImg);
//
//		/*imshow("test", croppedImg);
//		waitKey(0);*/
//
//		return croppedImg;
//	}
//	else
//	{
//		return;
//	}
//}

//vector<Mat> _slidingSameSizedWindow(Mat source, vector<Mat> &subimgs, Size size, int x_step, int y_step)
//{
//	int height = source.rows;
//	int width = source.cols;
//	Mat temp;
//
//	//cout << "size.height " << size.height << endl;
//	//cout << "size.width " << size.width << endl;
//
//	for(int ht = 0; ht < height - size.height; ht += y_step){
//		cout << "ht  " << ht << endl;
//		for(int wd = 0; wd < width - size.width; wd += x_step)
//		{
//			cout << "wd  " << wd << endl;
//			temp = _cropImage(source, size, Point(wd, ht));
//			subimgs.push_back(temp);
//			temp.release();
//		}
//	}
//
//	return subimgs;
//}

//vector<vector<Mat>> _slidingWindow(Mat source, vector<vector<Mat>> &collection, Size init_size, Size final_size, double factor, int x_step, int y_step)
//{
//	if(source.rows < init_size.height && source.cols < init_size.width)
//	{
//		init_size.height = source.rows;
//		init_size.width = source.cols;
//	}
//	
//	Size size = init_size;
//	
//	while(final_size.height < size.height && final_size.width < size.width)
//	{
//		vector<Mat> imgs;
//		_slidingSameSizedWindow(source, imgs, size, x_step, y_step);
//		collection.push_back(imgs);
//
//		size.height = size.height * factor;
//		size.width = size.width * factor;
//
//		//imgs.clear();
//	}
//
//	return collection;
//}




int _tmain(int argc, char* argv[])
{
	ClassInfo* trainInfo;
	ClassInfo* testInfo;

	//path
	argv[1] = "C:/Course/C plusplus test/test1/samples";
	argv[2] = "C:/Course/C plusplus test/hogtest/imgs";

	trainInfo = TraversClassFolder(argv[1]);
	testInfo = TraversClassFolder(argv[2]);

	double** trainFeatures;
	double** testFeatures;
	int* trainIndex;
	int* testIndex;
	int error = 0;

	trainFeatures = (double**)malloc(sizeof(double*) * trainInfo->fileCount);
	testFeatures = (double**)malloc(sizeof(double*) * testInfo->fileCount);
	trainIndex = (int*)malloc(sizeof(int) * trainInfo->fileCount);
	testIndex = (int*)malloc(sizeof(int) * testInfo ->fileCount);

	//declair HOG detector
	//cv::HOGDescriptor HOGdetector(Size(64,128), Size(16, 16), Size(8,8), Size(8,8),9,cv::gpu::HOGDescriptor::DEFAULT_WIN_SIGMA, 0.2,true, 128);
	cv::HOGDescriptor HOGdetector(Size(64,64), Size(16, 16), Size(8,8), Size(8,8),9,cv::gpu::HOGDescriptor::DEFAULT_WIN_SIGMA, 0.2,true, 128);

	//two dimensional array of storing descriptors' values of all examples
	vector<vector<float>> alldescriptors;

	//label
	vector<vector<float>> imgLabel;

	//load images
	for(int classcount = 0; classcount < trainInfo->classCount; classcount++)
	{
		for(int filecount = 0; filecount < trainInfo->fileCounts[classcount]; filecount++)
		{
			vector<float> tempLabel;
			
			//cout << "Class: " << classcount << "  Sample: " << filecount << endl;
			cout << "Class: " << classcount << "  Sample: " << filecount << endl;

			//load image
			Mat Img = imread(trainInfo->fileName[classcount][filecount].c_str(), CV_LOAD_IMAGE_COLOR);

			//resize image
			resize(Img, Img, Size(64,64));

			//convert image to gray scale
			cvtColor(Img, Img, CV_RGB2GRAY);

			//declair an array for descriptors' values and locations of each image
			vector<float> descriptorsValues;
			vector<Point> locations;

			//compute HOG detector
			HOGdetector.compute(Img, descriptorsValues, Size(0,0), Size(0,0), locations);

			//organize HOG feature
			alldescriptors.push_back(descriptorsValues);

			//assign label
			tempLabel.push_back((float)classcount);
			imgLabel.push_back(tempLabel);

			//clear vectors
			descriptorsValues.clear();
			locations.clear();
			tempLabel.clear();
		}
	}

	//put descriptors into Mat
	Mat trainingdata = _createMat(alldescriptors);
	Mat dataLabel = _createMat(imgLabel);

	//set SVM parameters
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.gamma = 0.0001;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	CvParamGrid CvParamGrid_C(0, 1, 0);
	CvParamGrid CvParamGrid_Gamma;
	Mysvm SVM;

	//training
	SVM.train_auto(trainingdata, dataLabel, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C)
			, CvParamGrid_C, CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU)
			, CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);
	
	//saving
	SVM.save("C:/Course/C plusplus test/hogtest/classifier_test.txt");

	int supportVectorSize = SVM.get_support_vector_count();
	cout << "support vector size of SVM： " << supportVectorSize << endl;

	CvMat *sv,*alp,*re; 
    sv  = cvCreateMat(supportVectorSize , 1764, CV_32FC1);
    alp = cvCreateMat(1 , supportVectorSize, CV_32FC1);
    re  = cvCreateMat(1 , 1764, CV_32FC1);
    CvMat *res  = cvCreateMat(1 , 1, CV_32FC1);

    cvSetZero(sv);
    cvSetZero(re);

    for(int i=0; i<supportVectorSize; i++)
    {
        memcpy( (float*)(sv->data.fl+i*1764), SVM.get_support_vector(i), 1764*sizeof(float));    
    }

    double* alphaArr = SVM.get_alpha();
    int alphaCount = SVM.get_alpha_count();

    for(int i=0; i<supportVectorSize; i++)
    {
        alp->data.fl[i] = (float)alphaArr[i];
    }
    cvMatMul(alp, sv, re);

    int posCount = 0;
    for (int i=0; i<1764; i++)
    {
        re->data.fl[i] *= -1;
    }


	FILE* fp = fopen("C:/Course/C plusplus test/hogtest/hogSVMDetector-birdnest.txt","wb");
    if( NULL == fp )
    {
        return 1;
    }
    for(int i=0; i<1764; i++)
    {
        fprintf(fp,"%f \n",re->data.fl[i]);
    }
    float rho = SVM.get_rho();
    fprintf(fp, "%f", rho);
    cout<<"C:/Course/C plusplus test/hogtest/hogSVMDetector-birdnest.txt done!"<<endl;
    fclose(fp);

	//test//
	//testInfo
	
	cout << endl << endl << testInfo->classCount << endl;
	for(int classcount = 0; classcount < testInfo->classCount; classcount++)
	{
		cout << "test" << endl;
		for(int filecount = 0; filecount < testInfo->fileCounts[classcount]; filecount++)
		{

			Mat s = imread(testInfo->fileName[classcount][filecount].c_str(), 1);

			resize(s, s, Size(1280,1280));

			vector<float> x;
			ifstream fileIn("C:/Course/C plusplus test/hogtest/hogSVMDetector-birdnest.txt", ios::in);
			float val = 0.0f;
			while(!fileIn.eof())
			{
				fileIn>>val;
				x.push_back(val);
			}
			fileIn.close();
			vector<cv::Rect>  found;
			cv::HOGDescriptor hog(cv::Size(64,64), cv::Size(16,16), cv::Size(8,8), cv::Size(8,8), 9);

			hog.setSVMDetector(x);

			//IplImage* s = NULL;
			//cvNamedWindow("img", 0);
			hog.detectMultiScale(s, found, 0.8, cv::Size(8,8), cv::Size(32,32), 1.05, 2);

			if (found.size() > 0)
			{
				for (int i=0; i<found.size(); i++)
				{
					CvRect tempRect = cvRect(found[i].x, found[i].y, found[i].width, found[i].height);

					rectangle(s, cvPoint(tempRect.x,tempRect.y),
						cvPoint(tempRect.x+tempRect.width,tempRect.y+tempRect.height),CV_RGB(255,0,0), 2);
				}
			}
			//imshow("people detector", s);
			char tcount[10];
			itoa(filecount, tcount, 10);
			string savename = "C:/Course/C plusplus test/hogtest/save/" + string(tcount) + ".jpg";

			cout << savename << " done  " << imwrite(savename, s) << endl;
			//cout << savename << " done  " << imwrite(savename, s) << endl;
			//waitKey();
		}
	}
	/*Size size(s.cols, s.rows);
	vector<vector<Mat>> collections;
	_slidingWindow(s, collections, size, Size(10, 10), 0.9, 30, 30);
	showOneImgfromCollection(collections, 0, 0);*/
	//cout << collections.at(0).size() << endl;
	/*_cropImage(s, size, Point(100,100));*/

	return 0;
}

