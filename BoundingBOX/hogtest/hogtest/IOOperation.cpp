#include "stdafx.h"
#include "IOOperation.h"

#define PATH_LENGTH 300


ClassInfo* TraversClassFolder(const char* path)
{
	ClassInfo* classInfo = new ClassInfo();
	vector<string> pathVec;
	vector<string> fileVec;
	string rootPath = path;
	rootPath.append("\\");

	classInfo->fileCount = 0;
	pathVec.push_back(rootPath);
	while(!pathVec.empty())
	{
		WIN32_FIND_DATAA findFileData;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		string searchPath = pathVec.front();
		searchPath.append("*");
		hFind = FindFirstFileA(searchPath.c_str(),&findFileData);
		if(hFind == INVALID_HANDLE_VALUE)
		{
			return classInfo;
		}

		fileVec.clear();
		int folderCount = 0;
		int fileCount = 0;
		while(FindNextFileA(hFind,&findFileData))
		{
			if(strcmp(findFileData.cFileName,"..")!=0 )
			{
				if(findFileData.dwFileAttributes == FILE_ATTRIBUTE_HIDDEN || findFileData.dwFileAttributes == 38 || findFileData.dwFileAttributes == FILE_ATTRIBUTE_SYSTEM)
					continue;
				if(findFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
				{
					int offset;
					string dir = findFileData.cFileName;
					string rootTemp = rootPath;
					dir = rootPath.append(dir);
					rootPath = rootTemp;
					dir.append("\\");
					pathVec.push_back(dir);
					dir.erase(dir.length()-1, 1);
					offset = dir.find_last_of("\\",dir.length()-1);
					dir = dir.substr(offset+1, dir.length() - offset);
					classInfo->className.push_back(dir);
					folderCount ++;
				}
				else if(findFileData.dwFileAttributes == FILE_ATTRIBUTE_NORMAL || findFileData.dwFileAttributes == FILE_ATTRIBUTE_ARCHIVE)
				{
					string file = findFileData.cFileName;
					string fileTemp = pathVec.front();
					file = fileTemp.append(file);
					fileVec.push_back(file);
					fileCount ++;
				}
			}
		}
		if(folderCount!=0)
			classInfo->classCount = folderCount;
		if(fileCount != 0)
		{
			classInfo->fileCounts.push_back(fileCount);
			classInfo->fileName.push_back(fileVec);
			classInfo->fileCount += fileCount;
		}
		pathVec.erase(pathVec.begin());
	}
	return classInfo;
}
