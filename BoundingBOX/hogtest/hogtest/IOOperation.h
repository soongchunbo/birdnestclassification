#include <vector>
#include <cstring>
#include "windows.h"
using namespace std;

#ifndef DCV_LIB_IO
#define DCV_LIB_IO

struct ClassInfo
{
	int classCount;
	int fileCount;
	vector<int> fileCounts;
	vector<string> className;
	vector<vector<string>> fileName;
};

ClassInfo* TraversClassFolder(const char* path);

#endif